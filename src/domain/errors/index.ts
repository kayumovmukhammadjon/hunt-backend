export const ErrorCodes = {
  10265: {
    statusCode: 409,
    code: 10265,
    message: "User already registered!",
  },
  10365: {
    statusCode: 409,
    code: 10365,
    message: "Phone number or password incorrect!",
  },
  10465: {
    statusCode: 409,
    code: 10465,
    message: "User doesn't exists",
  },
  10565: {
    statusCode: 409,
    code: 10565,
    message: "Old password incorrect!",
  },
  10665: {
    statusCode: 409,
    code: 10665,
    message: "Verification code incorrect!",
  },
  10765: {
    statusCode: 409,
    code: 10765,
    message: "Verification code expired!",
  },
  10865: {
    statusCode: 409,
    code: 10865,
    message: "Client doesn't exist!",
  },
  10965: {
    statusCode: 409,
    code: 11265,
    message: "Country doesn't exist!",
  },
  11065: {
    statusCode: 409,
    code: 11365,
    message: "Region doesn't exist!",
  },
  11165: {
    statusCode: 409,
    code: 11465,
    message: "Already exist card!",
  },
  11265: {
    statusCode: 409,
    code: 11265,
    message: "Avatar Id incorrect!",
  },
  11365: {
    statusCode: 409,
    code: 11365,
    message: "Currency Id incorrect!",
  },
  11465: {
    statusCode: 409,
    code: 11465,
    message: "Brand doesn't exist!",
  },
  11565: {
    statusCode: 409,
    code: 11565,
    message: "Please you must completed some steps: ${stepErrors}",
  },
  11665: {
    statusCode: 409,
    code: 11665,
    message: "Token already used!",
  },
  11765: {
    statusCode: 409,
    code: 11765,
    message: "Card already exists!",
  },
  11865: {
    statusCode: 409,
    code: 11865,
    message: "Card doesn't exist!",
  },
  11965: {
    statusCode: 409,
    code: 11965,
    message: "Card is expired!",
  },
  12065: {
    statusCode: 409,
    code: 12065,
    message: "Subscription doesn't exist!",
  },
  12165: {
    statusCode: 409,
    code: 12165,
    message: "You should add card!",
  },
  12265: {
    statusCode: 409,
    code: 12265,
    message: "Currency doesn't exists!",
  },
  12365: {
    statusCode: 409,
    code: 12365,
    message: "Action ${i} is invalid!",
  },
  12465: {
    statusCode: 409,
    code: 12465,
    message: "Verification code has expired. Please request a new code.",
  },
  12565: {
    statusCode: 409,
    code: 12565,
    message: "Phone number already exists!",
  },
  12665: {
    statusCode: 409,
    code: 12665,
    message: "Branch doesn't exist!",
  },
  12765: {
    statusCode: 409,
    code: 12765,
    message: "You must be configure payment!",
  },
  12865: {
    statusCode: 409,
    code: 12865,
    message: "[${notFoundRegions}] regions doesn't exists!",
  },
  12965: {
    statusCode: 409,
    code: 12965,
    message: "Image doesn't exist!",
  },
  13065: {
    statusCode: 409,
    code: 13065,
    message: "Parent category doesn't exist",
  },
  13165: {
    statusCode: 409,
    code: 13165,
    message: "${category.name} category childs must be only product!",
  },
  13265: {
    statusCode: 409,
    code: 13265,
    message: "This [${categoriesList}] categories doesn't exist",
  },
  13365: {
    statusCode: 409,
    code: 13365,
    message: "${brand.id} brand doesn't have ${branch.id} branch!",
  },
  13465: {
    statusCode: 409,
    code: 13465,
    message: "Driver doesn't exist!",
  },
  13565: {
    statusCode: 409,
    code: 13565,
    message: "Driver order doesn't exists",
  },
  13665: {
    statusCode: 409,
    code: 13665,
    message: "You can only start PERFORMER FOUND ORDERS",
  },
  13765: {
    statusCode: 409,
    code: 13765,
    message: "You can only start PICKUP ARRIVED ORDERS",
  },
  13865: {
    statusCode: 409,
    code: 13865,
    message: "You can only start PICKUPED ORDERS",
  },
  13965: {
    statusCode: 409,
    code: 13965,
    message: "You can only start DELIVERY ARRIVED ORDERS",
  },
  14065: {
    statusCode: 409,
    code: 14065,
    message: "Role doesn't exist!",
  },
  14165: {
    statusCode: 409,
    code: 14165,
    message: "Employee already exists!",
  },
  14265: {
    statusCode: 409,
    code: 14265,
    message: "Employee doesn't exist!",
  },
  14365: {
    statusCode: 409,
    code: 14365,
    message: "Old password doesn't match!",
  },
  14465: {
    statusCode: 409,
    code: 14465,
    message: "Card payment is temporarily unavailable!",
  },
  14565: {
    statusCode: 409,
    code: 14565,
    message: "Order delivery system is temporarily unavailable!",
  },
  14665: {
    statusCode: 409,
    code: 14665,
    message: "${brand.id} brand currency doesn't match ${currency.id}",
  },
  14765: {
    statusCode: 409,
    code: 14765,
    message: "Customer doesn't exist!",
  },
  14865: {
    statusCode: 409,
    code: 14865,
    message: "${productsError} products doesn't exist!",
  },
  14965: {
    statusCode: 409,
    code: 14965,
    message: "Order doesn't exists",
  },
  15065: {
    statusCode: 409,
    code: 15065,
    message: "Client card doesn't exist!",
  },
  15165: {
    statusCode: 409,
    code: 15165,
    message: "Order status must be accepted!",
  },
  15265: {
    statusCode: 409,
    code: 15265,
    message: "${brand.id} doesn't have ${branch.id} branch!",
  },
  15365: {
    statusCode: 409,
    code: 15365,
    message: "Customer cards must be entered!",
  },
  15465: {
    statusCode: 409,
    code: 15465,
    message: "Customer must be add card!",
  },
  15565: {
    statusCode: 409,
    code: 15565,
    message: "Customer cards doesn't exist!",
  },
  15665: {
    statusCode: 409,
    code: 15665,
    message: "Delivery type incorrect!",
  },
  15765: {
    statusCode: 409,
    code: 15765,
    message: "${category.name} category childs must be only category!",
  },
  15865: {
    statusCode: 409,
    code: 15865,
    message: "Product arrival price currency doesn't exist!",
  },
  15965: {
    statusCode: 409,
    code: 15965,
    message: "Product selling price currency doesn't exist!",
  },
  16065: {
    statusCode: 409,
    code: 16065,
    message:
      "Product selling price currency doesn't match brand initial currency!",
  },
  16165: {
    statusCode: 409,
    code: 16165,
    message: "Category doesn't exist!",
  },
  16265: {
    statusCode: 409,
    code: 16265,
    message: "${iterator.branchId} - branchId doesn't exist!",
  },
  16365: {
    statusCode: 409,
    code: 16365,
    message: "This [${productList}] products doesn't exist!",
  },
  16465: {
    statusCode: 409,
    code: 16465,
    message:
      "Product ${option} option selling price currency doesn't match brand initial currency!",
  },
};
