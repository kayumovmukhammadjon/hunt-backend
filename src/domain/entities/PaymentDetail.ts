import { ICurrency } from "./Currency";
import { IResume } from "./Resume";
import { IVacancy } from "./Vacancy";

export enum PaymentType {
    click = "click",
    payme = "payme"
}

export interface IPaymentDetail {
    id:string;
    createdAt:Date;
    updatedAt:Date;
    isDeleted:boolean;
    price:number;
    currency:ICurrency;
    vacancy:IVacancy;
    resume:IResume;
}