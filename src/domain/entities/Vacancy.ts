import { ICompany } from "./Company";
import { ICurrency } from "./Currency";
import { IPosition } from "./Position";
import { IRegion } from "./Region";
import { IUser } from "./User";

export enum Status {
    accepted="accepted",
    rejected="rejected",
    pending="pending",
}


export enum availability {
    day = "day",
    night = "night"
}

export enum Type {
    office="office",
    remote="remote"
}

export enum SalaryType {
    day = "day",
    week = "week",
    month = "month",
    year = "year"
}

export enum PaymentStatus {
    unpaid = "unpaid",
    paid = "paid",
}


export interface IVacancy {
    id:string;
    createdAt:Date;
    updatedAt:Date;
    index:number;
    user:IUser;
    company:ICompany;
    position:IPosition;
    status:Status;
    availability:availability;
    type:Type;
    workHours:{
        startTime:string;
        endTime:string;
    },
    salary:{
        price:string;
        currency:ICurrency,  
        type:SalaryType
    },
    experience:string;
    paymentStatus:PaymentStatus
    skills:string[];
    description:string;
    region:IRegion;
    isDeleted:boolean;
    language:string;
    contact:{
        username:string;
        phone:string;
        email:string;
    }
}