import {IFile} from "./File";
import { IUser } from "./User";

export interface INew{
    id:string;
    title:string;
    user:IUser;
    description:string;
    files:IFile[];
    isDeleted:boolean;
}