import {IFile} from "./File";
import { IUser } from "./User";

export interface IEpisode{
    id:string;
    title:string;
    user:IUser;
    description:string;
    files:IFile[];
    isDeleted:boolean;
}