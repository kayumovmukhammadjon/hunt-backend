import { IPaymentDetail } from "./PaymentDetail";

export enum ETransactionState {
    Paid =  "2",
    Pending= "1",
    PendingCanceled =  "-1",
    PaidCanceled= "-2",
}

export interface ITransaction {
    id: string;
    paymentDetail:IPaymentDetail;
    createdAta: Date;
    isDeleted: boolean;
    amount: number;
    state: ETransactionState;
    reason: number;
    transactionId:string;
    createTime:string;
    performTime:string;
    cancelTime:string;
    index:number;
}