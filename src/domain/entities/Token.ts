import { IUser } from './User'

export interface IToken {
    id: string
    createdAt: Date
    refreshToken: string
    accessToken: string
    refreshTokenExpireTime: Date
    accessTokenExpireTime: Date
    user: IUser
    isDeleted: boolean
}

export interface IGetUserByToken {
    userId: string
}
