import { IFile } from "./File";
import { IUser } from "./User";

export interface ICompany{
    id:string;
    user:IUser;
    name:string;
    file:IFile;
    phoneNumber:string;
    createdAt:Date;
    updatedAt:Date;
    isDeleted:boolean;
}