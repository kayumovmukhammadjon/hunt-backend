import { IRole } from "./Role";

export interface IUser {
  id: string;
  fullName: string;
  phoneNumber: string;
  username: string;
  chatId:string;
  verified: boolean;
  verificationCode: number;
  verificationCodeExpireTime: Date;
  isDeleted:boolean;
}
