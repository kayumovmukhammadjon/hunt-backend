export enum Roles {
    admin="admin",
    user="user"
}

export interface IRole {
    id:string;
    createdAt:Date;
    updatedAt:Date;
    isDeleted:boolean;
}