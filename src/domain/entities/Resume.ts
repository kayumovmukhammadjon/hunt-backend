import { IFile } from "./File";
import { IPosition } from "./Position";
import { IRegion } from "./Region";
import { IUser } from "./User";
import { ICurrency } from "./Currency";

export enum SalaryType {
    day = "day",
    week = "week",
    month = "month",
    year = "year"
}

export interface IResume {
    id:string;
    createdAt:Date;
    updatedAt:Date;
    index:number;
    user:IUser;
    file:IFile;
    language:string;
    region:IRegion;
    position:IPosition;
    contact:{
        username:string;
        phone:string;
        email:string;
    },
    salary:{
        price:string;
        currency:ICurrency, 
        type:SalaryType 
    },
    isDeleted:boolean;
}

