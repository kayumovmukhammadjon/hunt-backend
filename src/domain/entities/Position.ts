import { IFile } from "./File";
import { IUser } from "./User";

export interface IPosition {
    id:string;
    name:string;
    file:IFile;
    user:IUser;
    createdAt:Date;
    updatedAt:Date;
    isDeleted:boolean;
}

