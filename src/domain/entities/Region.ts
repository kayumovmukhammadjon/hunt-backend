import { ICountry } from "./Country";

export interface IRegion {
    id:string;
    country:ICountry;
    name:string;
    createdAt: Date;
    updatedAt: Date;
    isDeleted:boolean;
}