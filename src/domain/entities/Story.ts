import {IFile} from "./File";
import { IUser } from "./User";

export interface IStory{
    id:string;
    title:string;
    avatar:IFile;
    user:IUser;
    description:string;
    files:IFile[];
    isDeleted:boolean;
    schedulePostTime:Date;
}