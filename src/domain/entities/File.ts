export interface IFile {
  id: string;
  originalName: string;
  mimetype: string;
  extension: string;
  size: number;
  filename: string;
  isDeleted: boolean;
  createdAt: Date;
  updatedAt: Date;
}
