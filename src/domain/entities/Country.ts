export interface ICountry {
  id:string;
  name:string;
  emoji:string;
  unicode:string;
  image:string;
  code:string;
  isDeleted:boolean;
}