import {Router} from "express";

import CountryRoutes from "../modules/country/routes/index";
import RegionRoutes from "../modules/region/routes/index";
import UserRoutes from "../modules/users/routes/index";
import AuthRoutes from "../modules/auth/routes/index";
import FileRoutes from "../modules/files/routes/index";
import CurrencyRoutes from "../modules/currency/routes/index";
import EpisodeRoutes from "../modules/users/submodules/episodes/routes/index";
import NewsRoutes from "../modules/users/submodules/news/routes/index";

const router = Router({ mergeParams: true });

router.use("/countries",CountryRoutes);
router.use("/regions", RegionRoutes);
router.use("/users",UserRoutes);
router.use("/auth",AuthRoutes);
router.use("/files",FileRoutes);
router.use("/currencies",CurrencyRoutes);
router.use("/episodes",EpisodeRoutes);
router.use("/news",NewsRoutes);

export default router;
