import winston, { createLogger, transports, format, loggers } from "winston";
import { MongoDB } from "winston-mongodb";
import { configurations } from "../config/index";

const logFormat = format.combine(
  format.timestamp(),
  format.printf(({ createdAt, level, message }) => {
    return `${createdAt} ${level}: ${message}`;
  })
);

const logger = createLogger({
  level: "error",
  format: logFormat,
  transports: [
    new transports.Console(),

    new MongoDB({
      level: "info",
      db: configurations.mongodb.url,
      collection: "logs",
      options: {
        useUnifiedTopology: true,
      },
    }),
  ],
});
export default logger;
