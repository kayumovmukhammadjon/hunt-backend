async function setWebHook(bot: any, url: string) {
  return await bot.api
    .setWebhook(url + "/bot/")
    .then((res: any) => {
      console.log(res + " webhook set");
      return res;
    })
    .catch((err: any) => {
      console.error(err);
    });
}

export { setWebHook };
