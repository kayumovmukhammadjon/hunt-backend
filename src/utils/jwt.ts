import jwt from 'jsonwebtoken'
import { IGetUserByToken } from '../domain/entities/Token'

export const generateToken = (
    payLoad: IGetUserByToken,
    secret: string,
    expiresIn: number
) => {
    return jwt.sign(payLoad, secret, {
        expiresIn,
    })
}

export const verifyToken = (token: string, secret: string) => {
    const tokenBody = token.slice(7)
    return jwt.verify(tokenBody, secret)
}
