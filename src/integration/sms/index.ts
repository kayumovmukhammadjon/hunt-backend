import axios from "axios";
import ObjectID from "bson-objectid";
import { configurations } from "../../config";

class SmsService {
  sendSms(recipient: string, text: string) {
    return axios.post(
      configurations.smsService.api,
      {
        messages: [
          {
            recipient,
            "message-id": new ObjectID(20),
            sms: {
              originator: 3700,
              content: {
                text,
              },
            },
          },
        ],
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
        auth: {
          username: configurations.smsService.login,
          password: configurations.smsService.password,
        },
      }
    );
  }
  sendTelegramMessage(text: string) {
    return axios.post(
      `https://api.telegram.org/bot6522831116:AAFNzmWkVj10i5tawbkPKmxKpK3zYiFwvDs/sendMessage?chat_id=-1001997758066&text=${text}`
    );
  }
}

export default new SmsService();
