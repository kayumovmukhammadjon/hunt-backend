import { config as dotenv } from "dotenv";

dotenv();

const OptochkaPaymeCashboxs = {
  optoDelivery: {
    merchantId: process.env.PAYME_OPTODELIVERY_MERCHANT_ID || "",
    merchantKey: process.env.PAYME_OPTODELIVERY_MERCHANT_KEY || "",
    paymeUrl: process.env.PAYME_BASE_URL || "",
  },
  optochka: {
    merchantId: process.env.PAYME_OPTOCHKA_MERCHANT_ID || "",
    merchantKey: process.env.PAYME_OPTOCHKA_MERCHANT_KEY || "",
    paymeUrl: process.env.PAYME_BASE_URL || "",
  },
};

export default OptochkaPaymeCashboxs;
