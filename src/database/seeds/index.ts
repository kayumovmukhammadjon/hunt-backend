import fs from "fs";
import Country from "../models/Country";
import Region from "../models/Region";
import Currency from "../models/Currency";
import Database from "../core/index";

export const currencySeeder = async () => {
  const jsonData: any = fs.readFileSync(__dirname + "/currency.json");
  const data = JSON.parse(jsonData);

  for (let i = 0; i < data.currencies.length; i++) {
    await Currency.create(data.currencies[i]);
  }

  console.log(`finished currencySeeder`);
};

export const countrySeeder = async () => {
    const jsonData:string = fs.readFileSync("src/database/seeds/country.json","utf-8");

    const data = JSON.parse(jsonData);

    const countriesToInsert = data.countries.map((country:any) => {
        Country.create(country);
    });
    
    console.log("Countries inserted");
}

export const regionSeeder = async () => {
  const jsonData:string = fs.readFileSync("src/database/seeds/region.json","utf-8");

  const data = JSON.parse(jsonData);

  const regionsToInsert = data.map(async (region:any) => {
      console.log("region",region);
      const country = await Country.findOne({
        name: region.countryName
      });



      region.regions.map(async (region:any) => {
        Region.create({
          name:region.name,
          country:country,
        });
      });
  });

  console.log("Regions inserted");

}


(async function () {
    try {
      const db = new Database();
  
      db.connect();
      // currencySeeder();
      // countrySeeder();
      // regionSeeder()
    } catch (e: any) {
      throw new Error(e);
    }
  })();






