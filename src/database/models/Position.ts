import { Schema,model } from "mongoose";
import { IPosition } from "../../domain/entities/Position";

const PositionSchema = new Schema<IPosition>({
    name:{
        type:String,
        required:true
    },
    user:{
        type:Schema.Types.ObjectId,
        ref:'User'
    },
    file:{
        type:Schema.Types.ObjectId,
        ref:'File'
    },
    isDeleted:{
        type:Boolean,
        default:false
    }
})

export default model<IPosition>('Position',PositionSchema);
