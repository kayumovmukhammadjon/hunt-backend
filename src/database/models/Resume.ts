import {IResume} from "../../domain/entities/Resume";
import {Schema,model} from "mongoose";

const ResumeSchema = new Schema<IResume>({
    index:{
        type:Number
    },
    user:{
        type:Schema.Types.ObjectId,
        ref:"User"
    },
    file:{
        type:Schema.Types.ObjectId,
        ref:"File"
    },
    isDeleted:{
        type:Boolean,
        default:false
    },
    region:{
        type:Schema.Types.ObjectId,
        ref:"Region"
    },
    position:{
        type:Schema.Types.ObjectId,
        ref:"Position"
    },
    language:{
        type:String
    },
    contact:{
        username:{
            type:String
        },
        phone:{
            type:String
        },
        email:{
            type:String
        }
    }
},{
    versionKey: false,
    timestamps: true,
    toObject: {
      virtuals: true,
    },
    toJSON: {
      virtuals: true,
    },
});

export default model<IResume>("Resume",ResumeSchema);