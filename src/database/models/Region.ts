import { Schema, model } from 'mongoose';
import { IRegion } from '../../domain/entities/Region';

const RegionSchema = new Schema<IRegion>({
    name: {
        type: String,
        required: true,
    },
    country: {
        type: Schema.Types.ObjectId,
        ref: 'Country',
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
}, {
    versionKey: false,
    timestamps: true,
    toObject: {
        virtuals: true,
    },
    toJSON: {
        virtuals: true,
    },
});

export default model<IRegion>('Region', RegionSchema);