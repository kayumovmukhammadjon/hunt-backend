import { Schema,model } from "mongoose"
import {ITransaction} from "../../domain/entities/Transaction";

const TransactionSchema = new Schema<ITransaction>
    ({
        amount: {
            type: Number,
        },
        paymentDetail:{
            type: Schema.Types.ObjectId,
            ref: "PaymentDetail",
        },
        state: {
            type: String,
        },
        reason: {
            type: Number,
        },
        transactionId: {
            type: String,
        },
        createTime: {
            type: String,
        },
        performTime: {
            type: String,
        },
        cancelTime: {
            type: String,
        },
        isDeleted:{
            type: Boolean,
            default: false,
        },
    },
    {
        versionKey: false,
        timestamps: true,
        toObject: {
          virtuals: true,
        },
        toJSON: {
          virtuals: true,
        },
    }
);

export default model<ITransaction>("Transaction", TransactionSchema);

