import {Schema,model} from "mongoose";
import { ICompany } from "../../domain/entities/Company";

const CompanySchema = new Schema<ICompany>({
    name:{
        type:String,
        required:true
    },
    file:{
        type:Schema.Types.ObjectId,
        ref:'File'
    },
    user:{
        type:Schema.Types.ObjectId,
        ref:'User'
    },
    phoneNumber:{
        type:String
    },
    isDeleted:{
        type:Boolean,
        default:false
    }
})

export default model<ICompany>('Company',CompanySchema);

