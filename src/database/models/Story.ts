import {Schema,model} from 'mongoose';
import {IStory} from "../../domain/entities/Story";

const StorySchema = new Schema<IStory>({
    description:{
        type:String,
        required:true
    },
    
    files:[{
        type:Schema.Types.ObjectId,
        ref:'File'
    }],
    avatar:{
        type:Schema.Types.ObjectId,
        ref:'File'
    },
    title:{
        type:String
    },
    user:{
        type:Schema.Types.ObjectId,
        ref:"User"
    },
    isDeleted:{
        type:Boolean,
        default:false
    },
    schedulePostTime:{
        type:Date
    }
},{
    versionKey: false,
    timestamps: true,
    toObject: {
        virtuals: true,
    },
    toJSON: {
        virtuals: true,
    },
});

export default model<IStory>('Story',StorySchema);

