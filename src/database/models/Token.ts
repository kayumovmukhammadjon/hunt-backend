import { IToken } from '../../domain/entities/Token'
import { Schema, model } from 'mongoose'

const TokenSchema = new Schema<IToken>(
    {
        refreshToken: {
            type: String,
            required: true,
        },
        accessToken: {
            type: String,
            required: true,
        },
        refreshTokenExpireTime: {
            type: Date,
        },
        accessTokenExpireTime: {
            type: Date,
        },
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User',
        },
        isDeleted: {
            type: Boolean,
            default: false,
        },
    },
    {
        versionKey: false,
        timestamps: true,
        toObject: {
            virtuals: true,
        },
        toJSON: {
            virtuals: true,
        },
    }
)

export default model<IToken>('Token', TokenSchema)
