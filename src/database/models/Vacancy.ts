import {IVacancy, PaymentStatus, Status} from "../../domain/entities/Vacancy";
import {Schema,model} from "mongoose";

const vacancySchema = new Schema<IVacancy>({
    index:{
        type:Number
    },
    company:{
        type:Schema.Types.ObjectId,
        ref:"Company"
    },
    region:{
        type:Schema.Types.ObjectId,
        ref:"Region"
    },
    position:{
        type:Schema.Types.ObjectId,
        ref:"Position"
    },
    user:{
        type:Schema.Types.ObjectId,
        ref:"User"
    },
    status:{
        type:String,
        enum:["accepted","rejected","pending"],
        default:Status.pending
    },
    availability:{
        type:String
    },
    type:{
        type:String
    },
    workHours:{
        startTime:{
            type:String
        },
        endTime:{
            type:String
        }
    },
    salary:{
        price:{
            type:String
        },
        currency:{
            type:Schema.Types.ObjectId,
            ref:"Currency"
        },  
        type:{
            type:String
        }
    },
    experience:{
        type:String
    },
    paymentStatus:{
        type:String,
        enum:["unpaid","paid"],
        default:PaymentStatus.unpaid
    },
    skills:[
        {
            type:String
        }
    ],
    description:{
        type:String
    },
    isDeleted:{
        type:Boolean,
        default:false
    },
    language:{
        type:String
    },
    contact:{
        username:{
            type:String
        },
        phone:{
            type:String
        },
        email:{
            type:String
        }
    }
},{
    versionKey: false,
    timestamps: true,
    toObject: {
      virtuals: true,
    },
    toJSON: {
      virtuals: true,
    },
});

export default model<IVacancy>("Vacancy",vacancySchema);