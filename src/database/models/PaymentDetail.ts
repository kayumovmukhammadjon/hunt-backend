import {Schema,model} from "mongoose";
import { IPaymentDetail } from "../../domain/entities/PaymentDetail";

const PaymentDetailSchema = new Schema<IPaymentDetail>({
    price:{
        type:Number
    },
    currency:{
        type: Schema.Types.ObjectId,
        ref: "Currency",
    },
    vacancy:{
        type: Schema.Types.ObjectId,
        ref: "Vacancy",
    },
    resume:{
        type: Schema.Types.ObjectId,
        ref: "Resume",
    }
},{
    versionKey: false,
    timestamps: true,
    toObject: {
      virtuals: true,
    },
    toJSON: {
      virtuals: true,
    },
});

export default model<IPaymentDetail>("PaymentDetail",PaymentDetailSchema)