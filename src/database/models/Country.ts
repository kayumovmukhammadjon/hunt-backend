import {ICountry} from "../../domain/entities/Country";
import {Schema,model} from "mongoose";

const CountrySchema = new Schema<ICountry>({
    name:{
        type:String,
        required:true
    },
    emoji:{
        type:String,
        required:true
    },
    unicode:{
        type:String,
        required:true
    },
    image:{
        type:String,
        required:true
    },
    code:{
        type:String,
        required:true
    },
    isDeleted:{
        type:Boolean,
        default:false
    }
},{
    versionKey: false,
    timestamps: true,
    toObject: {
        virtuals: true,
    },
    toJSON: {
        virtuals: true,
    },
});

export default model<ICountry>("Country",CountrySchema);

