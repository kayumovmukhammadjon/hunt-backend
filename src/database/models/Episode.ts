import { Schema,model } from "mongoose";
import {IEpisode} from "../../domain/entities/Episode";

const episodeSchema = new Schema<IEpisode>({
    description:{
        type:String,
        required:true
    },
    
    files:[{
        type:Schema.Types.ObjectId,
        ref:'File'
    }],
    title:{
        type:String
    },
    user:{
        type:Schema.Types.ObjectId,
        ref:"User"
    },
    isDeleted:{
        type:Boolean,
        default:false
    }
},{
    versionKey: false,
    timestamps: true,
    toObject: {
      virtuals: true,
    },
    toJSON: {
      virtuals: true,
    },
});

export default model<IEpisode>("Episode",episodeSchema);