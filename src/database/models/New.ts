import { Schema,model } from "mongoose";
import {INew} from "../../domain/entities/New";

const newSchema = new Schema<INew>({
    description:{
        type:String,
        required:true
    },
    
    files:[{
        type:Schema.Types.ObjectId,
        ref:'File'
    }],
    title:{
        type:String
    },
    user:{
        type:Schema.Types.ObjectId,
        ref:"User"
    },
    isDeleted:{
        type:Boolean,
        default:false
    }
},{
    versionKey: false,
    timestamps: true,
    toObject: {
      virtuals: true,
    },
    toJSON: {
      virtuals: true,
    },
});

export default model<INew>("New",newSchema);