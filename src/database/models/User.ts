import { Schema, model } from "mongoose";
import { IUser } from "../../domain/entities/User";

const UserSchema = new Schema<IUser>(
  {
    fullName: {
      type: String,
    },
    username: {
      type: String,
    },
    phoneNumber: {
      type: String,
    },
    chatId:{
      type: String,
    },
    verified: {
      type: Boolean,
      default: false,
    },
    verificationCode: {
      type: Number,
    },
    verificationCodeExpireTime: {
      type: Date,
    },
    isDeleted: {
      type: Boolean,
      default: false,
  },
  },
  {
    versionKey: false,
    timestamps: true,
    toObject: {
      virtuals: true,
    },
    toJSON: {
      virtuals: true,
    },
  }
);

export default model<IUser>("User", UserSchema);
