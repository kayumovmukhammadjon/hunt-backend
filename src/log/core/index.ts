import winston from "winston";
import winsTonMongoDb from "winston-mongodb";
import { LogLevel } from "../enums/log-levels";

class WinstonLogger implements Logger {
  private logger: winston.Logger;

  constructor(collectionName: string) {
    this.logger = winston.createLogger({
      level: LogLevel.info,
      format: winston.format.combine(winston.format.timestamp()),
      transports: [
        new winston.transports.MongoDB({
          db: "mongodb://localhost:27017/logs", // MongoDB connection URI
          collection: collectionName, 
          options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
          },
        }),
      ],
    });
  }

  debug(message: string, ...args: any[]): void {
    this.logger.debug(message, ...args);
  }

  info(message: string, ...args: any[]): void {
    this.logger.info(message, ...args);
  }

  warn(message: string, ...args: any[]): void {
    this.logger.warn(message, ...args);
  }

  error(message: string, ...args: any[]): void {
    this.logger.error(message, ...args);
  }
}

export default WinstonLogger;
