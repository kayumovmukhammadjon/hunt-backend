import WinstonLogger from "../../core";
import { CustomerCollection, ICustomerLogMessages } from "./enum";
import { TLoginAndLogOut } from "./interface";

const logger = new WinstonLogger(CustomerCollection);

class CustomerLogger {
  loginAndLogOut(data: TLoginAndLogOut) {
    logger.info(ICustomerLogMessages.loginAndLogOut, data);
  }
}

export default new CustomerLogger();
