export type TLoginAndLogOut = {
  loginDate: Date;
  durationMinutes: number;
  logoutDate: Date;
};
