const jwt = require("jsonwebtoken");
import { NextFunction, Request, Response } from "express";
import { configurations } from "../config";
import  { jwtDecode } from 'jwt-decode';

const authenticateToken = (req: any, res: Response, next: NextFunction) => {
  const token = req.header("Authorization");

  const {userId} = req.query;

  if (!token) {
    return res.status(401).json({
      code: 401,
      message: "Authentication failed. Token not provided.",
    });
  }

  jwt.verify(
    token,
    configurations.jwt.accessTokenSecret,
    async (err: any, data: any) => {
      if (err) {
        return res.status(401).json({
          code: 401,
          message: "Authentication failed. Invalid token.",
        });
      }

      req.user = data;
      next();
    }
  );


};

export default authenticateToken;
