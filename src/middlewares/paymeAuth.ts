import { NextFunction, Request, Response } from "express";
import paymeErrors from "../integration/payme/constants/payme-errors";
import { configurations } from "../config";


const paymeAuth = (req: any, res: Response, next: NextFunction) => {
  const authHeader = req.header("Authorization");
  const token = authHeader && authHeader.split(' ')[1];
  const {id} = req.body;

    if (!token) {
        return {
            id:id,
            error:{
                code: paymeErrors.InvalidAuthorization.code,
                message: paymeErrors.InvalidAuthorization.message.ru
            }
        }
    }

    const data = Buffer.from(token, 'base64').toString('utf8');

    if(!data.includes(configurations.payme.optochkaMerchantKey)){
        return {
            id:id,
            error:{
                code: paymeErrors.InvalidAuthorization.code,
                message: paymeErrors.InvalidAuthorization.message.ru
            }
        }
    }
    next();
};

export default paymeAuth;
