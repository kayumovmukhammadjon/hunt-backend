type Application = {
  port: string | "";
  host: string | "";
  name: string | "";
  env: string | "";
};

type MongoDB = {
  url: string | "";
};

type Bot = {
  token:string | "",
}

type JWT = {
  accessTokenSecret: string;
  refreshTokenSecret: string;
  accessTokenExpiresIn: string;
  refreshTokenExpiresIn: string;
};

type SmsService = {
  api: string;
  login: string;
  password: string;
};

type AWSService = {
  name: string;
  region: string;
  accessKey: string;
  secretKey: string;
  endpoint: string;
  url: string;
};

type Payme = {
  merchantId: string;
  merchantKey: string;
  baseUrl: string;
};

type Redis = {
  port: string;
  host: string;
  db: string;
  password: string;
};

type RabbitMQ = {
  url: string;
  host: string;
  port: string;
  username: string;
  password: string;
  queueName: string;
};


export type Config = {
  application: Application;
  mongodb: MongoDB;
  jwt: JWT;
  smsService: SmsService;
  awsService: AWSService;
  payme: Payme;
  rabbitMQ: RabbitMQ;
  redis: Redis;
  bot:Bot;
};
