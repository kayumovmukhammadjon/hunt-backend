import { config as dotenv } from "dotenv";
import { Config } from "./interface";

dotenv();

export const configurations: Config = {
  application: {
    port: process.env.APP_PORT || "",
    host: process.env.APP_HOST || "",
    name: process.env.APP_NAME || "",
    env: process.env.NODE_ENV || '',
  },
  bot:{
    token:process.env.BOT_TOKEN || ""
  },
  mongodb: {
    url: process.env.MONGO_URL || "",
  },
  jwt: {
    accessTokenExpiresIn: process.env.ACCESS_TOKEN_EXPIRES_IN || "",
    accessTokenSecret: process.env.ACCESS_TOKEN_SECRET || "",
    refreshTokenExpiresIn: process.env.REFRESH_TOKEN_EXPIRES_IN || "",
    refreshTokenSecret: process.env.REFRESH_TOKEN_SECRET || "",
  },
  smsService: {
    api: process.env.SMS_SERVICE_API || "",
    login: process.env.SMS_SERVICE_LOGIN || "",
    password: process.env.SMS_SERVICE_PASSWORD || "",
  },
  awsService: {
    name: process.env.DO_BUCKET_NAME || "",
    region: process.env.DO_BUCKET_REGION || "",
    accessKey: process.env.DO_ACCESS_KEY || "",
    secretKey: process.env.DO_SECRET_KEY || "",
    endpoint: process.env.DO_ENDPOINT || "",
    url: process.env.DO_URL || "",
  },
  payme: {
    merchantId: process.env.PAYME_MERCHANT_ID || "",
    merchantKey: process.env.PAYME_MERCHANT_KEY || "",
    baseUrl: process.env.PAYME_BASE_URL || "",
  },
  rabbitMQ: {
    url: process.env.RABBITMQ_URL || "",
    host: process.env.RABBITMQ_HOST || "",
    port: process.env.RABBITMQ_PORT || "",
    username: process.env.RABBITMQ_USERNAME || "",
    password: process.env.RABBITMQ_PASSWORD || "",
    queueName: process.env.RABBITMQ_QUEUE_NAME || "",
  },
  redis: {
    db: process.env.REDIS_DB || "",
    host: process.env.REDIS_HOST || "",
    port: process.env.REDIS_PORT || "",
    password: process.env.REDIS_PASSWORD || "",
  },
};
