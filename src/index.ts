import Database from "./database/core";
import "./modules/bot";
import server from "./server";
import { configurations } from "./config";
import { startBot } from "./modules/bot";

const PORT = configurations.application.port || 4000;

(async () => {
  try {
    const db = new Database();

    db.connect();

    startBot();

    server.listen(PORT, () => {
      console.log(`Server listening on port ${PORT}`);
    });

    process.on('SIGINT', function () {
      console.log('\nGracefully shutting down from SIGINT (Ctrl-C)')
      process.exit(0)
  })
  } catch (error) {
    throw new Error(`Error: ${error}`);
  }
})();
