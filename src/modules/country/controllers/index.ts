import { Request, Response } from "express";
import CountryServcei from "../service/service";

class TaskController {
  async getCountries(req: Request, res: Response) {
    try {
      const {
        code,
        message,
        data: result,
      } = await CountryServcei.getCountries();

      res.status(code).send({
        code,
        msg: message,
        data: result,
      });
    } catch (error) {
      res.status(500).send({ msg: "SERVER_ERROR", data: null });
      throw new Error(`Task controller [create] error: ${error}`);
    }
  }
}

export default new TaskController();
