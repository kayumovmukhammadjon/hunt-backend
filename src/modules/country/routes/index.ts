import { Router } from "express";
import CountryController from "../controllers/index";

const router = Router({ mergeParams: true });

const ctrl = CountryController;

router.route("/").get(ctrl.getCountries);

export default router;
