import { configurations } from "../../../config";
import logger from "../../../shared/logger";
import { ok, fail } from "../../../utils/response";
import CountryRepo from "../repo/repo";
import { TGetCountryById } from "./types";

const repo = new CountryRepo();

class CountryService {
  async getCountryById(data: TGetCountryById) {
    try {
      const { id } = data;

      return repo.getCountryById({ id });
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "country.service",
          method: "GET",
          methodName: "getCountryById",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [country.service] getCountryById"
      );
      console.log(`ERROR: [country.service] getCountryById: ${error}`);
      throw error;
    }
  }

  async getCountries() {
    try {
      const countries = await repo.getCountries();

      return ok(countries);
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "country.service",
          method: "GET",
          methodName: "getCountries",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: `countries `,
          apiData: {
            body: null,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [country.service] getCountries"
      );
      console.log(`ERROR: [country.service] getCountries: ${error}`);
      return fail(500, error);
    }
  }
}

export default new CountryService();
