import { configurations } from "../../../config";
import Country from "../../../database/models/Country";
import logger from "../../../shared/logger";
import { TGetCountryById } from "./types";

class CountryRepo {
  async getCountryById(data: TGetCountryById) {
    try {
      const { id } = data;

      return await Country.findOne({ _id: id, isDeleted: false });
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          repoName: "country.repo",
          method: "GET",
          methodName: "getCountryById",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [country.repo] getCountryById"
      );
      console.log(`ERROR: [country.repo] getCountryById: ${error}`);
      throw error;
    }
  }

  async getCountries() {
    try {
      return await Country.find({ isDeleted: false });
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          repoName: "country.repo",
          method: "GET",
          methodName: "getCountries",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: `countries `,
          apiData: {
            body: null,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [country.repo] getCountries"
      );
      console.log(`ERROR: [country.repo] getCountries: ${error}`);
      throw error;
    }
  }
}

export default CountryRepo;
