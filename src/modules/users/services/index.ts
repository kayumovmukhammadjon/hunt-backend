import { TCreateUser, TDeleteUser, TGetAllUsers, TGetUserById, TGetUserByPhoneNumber, TUpdateFullNameAndUsername, TUpdateUser, TUpdateUserVerification, TUpdateVerificationCode } from "./types";
import { configurations } from "../../../config";
import logger from "../../../shared/logger";
import UserRepo from "../repo";
import { fail,ok } from "../../../utils/response";

const repo = new UserRepo();

class UserService {
    async createUser(data: TCreateUser) {
        try {
            const newUser = await repo.create(data);

            return newUser
        } catch (error:any) {
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "users.service",
                  method: "POST",
                  methodName: "createUser",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `clients`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [users.service] createUser"
              );
              console.log(`ERROR: [users.service] createUser: ${error}`);
        }
    }

    async getUserById(data:TGetUserById){
        try{
            return await repo.getUserById({
                userId:data.userId
            });
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "users.service",
                  method: "GET",
                  methodName: "getUserById",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `getUserById`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [users.service] getUserById"
              );
            console.log(`ERROR: [users.service] getUserById: ${error}`);
        }
    }

    async getAllUsers(data:TGetAllUsers){
        try{
            const allUsers =  await repo.getAllUsers(data);

            return ok(allUsers);
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "users.service",
                  method: "GET",
                  methodName: "getAllUsers",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `getAllUsers`,
                  apiData: {
                    body: null,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [users.service] getAllUsers"
              );
            console.log(`ERROR: [users.service] getAllUsers: ${error}`);
            return fail(500,error);
        }
    }

    async getUser(data:TGetUserById){
        try{
            const user = await repo.getUserById({
                userId:data.userId
            });

            if(!user){
                return fail(404,"User Not Found!");
            }

            return ok(user);
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "users.service",
                  method: "GET",
                  methodName: "getUser",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `getUser`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [users.service] getUser"
              );
            console.log(`ERROR: [users.service] getUser: ${error}`);
            return fail(500, error);0
        }
    }

    async getUserByPhoneNumber(data:TGetUserByPhoneNumber){
        try{
            return await repo.getUserByPhoneNumber(data);
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "users.service",
                  method: "GET",
                  methodName: "getUserByPhoneNumber",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `getUserByPhoneNumber`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [users.service] getUserByPhoneNumber"
              );
            console.log(`ERROR: [users.service] getUserByPhoneNumber: ${error}`);
        }
    }

    async updateUser(data:TUpdateUser){
        try{
            const user = await repo.getUserById({
                userId:data.userId
            })

            if(!user){
                return fail(404,"User Not Found!");
            }
            
            data.user = user;

            const updatedUser =  await repo.updateUser(data);

            return ok(updatedUser);
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "users.service",
                  method: "PUT",
                  methodName: "updateUser",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `updateUser`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [users.service] updateUser"
              );
            console.log(`ERROR: [users.service] updateUser: ${error}`);
            return fail(500,error);
        }
    }

    async deleteUser(data:TDeleteUser){
        try{
            const user = await repo.getUserById({
                userId:data.userId
            });

            if(!user){
                return fail(404,"User Not Found");
            }

            data.user = user;

            const deletedUser =  await repo.deleteUser(data);

            return ok(deletedUser);
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "users.service",
                  method: "DELETE",
                  methodName: "deleteUser",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `deleteUser`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [users.service] deleteUser"
              );
            console.log(`ERROR: [users.service] deleteUser: ${error}`);
            return fail(500,error)
        }
    }

    async updateFullNameAndUsername(data:TUpdateFullNameAndUsername){
      try{
        console.log("data",data);
        return await repo.updateFullNameAndUsername(data);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "users.service",
            method: "PUT",
            methodName: "updateFullNameAndUsername",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: `updateFullNameAndUsername`,
            apiData: {
              body: data,
              query: null,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [users.service] updateFullNameAndUsername"
        );
      console.log(`ERROR: [users.service] updateFullNameAndUsername: ${error}`);
      return fail(500,error)
      }
    }

    async updateUserVerificationCode(data: TUpdateVerificationCode) {
      try{
        return await repo.updateUserVerificationCode(data)
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "users.service",
            method: "PUT",
            methodName: "updateUserVerificationCode",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: `updateUserVerificationCode`,
            apiData: {
              body: data,
              query: null,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [users.service] updateUserVerificationCode"
        );
      console.log(`ERROR: [users.service] updateUserVerificationCode: ${error}`);
      }
  }

  async updateUserVerification(data: TUpdateUserVerification) {
    try{
      return await repo.updateUserVerification(data);
    }catch(error:any){
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "users.service",
          method: "PUT",
          methodName: "updateUserVerification",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: `updateUserVerification`,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [users.service] updateUserVerification"
      );
    console.log(`ERROR: [users.service] updateUserVerification: ${error}`);
    }
  }
}


export default new UserService();