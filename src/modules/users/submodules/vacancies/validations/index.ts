import Joi from "joi";

interface VacancySchema {
    createVacancy:Joi.Schema<any>;
    updateVacancy:Joi.Schema<any>;
    deleteVacancy:Joi.Schema<any>
    getAllVacancies:Joi.Schema<any>
    getVacancy:Joi.Schema<any>
}

const schema:VacancySchema = {
    createVacancy:Joi.object().keys({
        body:Joi.object().keys({
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            regionId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            companyId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            positionId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            availability:Joi.string().required(),
            type:Joi.string().required(),
            workHours:Joi.object().keys({
                startTime:Joi.date().optional(),
                endTime:Joi.date().optional(),
            }),
            salary:Joi.object().keys({
                price:Joi.string().required(),
                currencyId:Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
                type:Joi.string().required()
            }),
            experience:Joi.string().required(),
            skills:Joi.array()
            .items(
              Joi.string()
                .optional()
            ),
            description:Joi.string().required(),
            language:Joi.string().required(),
            contact:Joi.object().keys({
                username:Joi.string().optional(),
                phone:Joi.string().optional(),
                email:Joi.string().optional(),
            }),
        }).required()
    }),
    updateVacancy:Joi.object().keys({
        body:Joi.object().keys({
            vacancyId:Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            regionId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            companyId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            positionId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            availability:Joi.string().required(),
            type:Joi.string().required(),
            workHours:Joi.object().keys({
                startTime:Joi.date().optional(),
                endTime:Joi.date().optional(),
            }),
            salary:Joi.object().keys({
                price:Joi.string().required(),
                currencyId:Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
                type:Joi.string().required()
            }),
            experience:Joi.string().required(),
            skills:Joi.array()
            .items(
              Joi.string()
                .optional()
            ),
            description:Joi.string().required(),
            language:Joi.string().required(),
            contact:Joi.object().keys({
                username:Joi.string().optional(),
                phone:Joi.string().optional(),
                email:Joi.string().optional(),
            }),
        }).required()
    }),
    deleteVacancy:Joi.object().keys({
        query:Joi.object().keys({
            vacancyId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
        }).required()
    }),
    getAllVacancies:Joi.object().keys({
        query:Joi.object().keys({
            regionId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .optional(),
            positionId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .optional(),
            experience:Joi.string().optional(),
            type:Joi.string().optional(),
            availability:Joi.string().optional(),
            limit: Joi.string().required(),
            skip:Joi.string().required(),
        }).required()
    }),
    getVacancy:Joi.object().keys({
        query:Joi.object().keys({
            vacancyId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
        }).required()
    }),
}

export default schema;