import { Router } from "express";
import ValidationRequest from "../../../../../middlewares/ValidationRequest";
import requestSchema from "../validations/index";
import auth from "../../../../../middlewares/Auth";

import UserController from "../controllers/index";


const router = Router({mergeParams:true});
const ctrl = UserController;

router
    .route("/")
    .post(ValidationRequest(requestSchema.createVacancy),auth,ctrl.createVacancy)
    .get(ValidationRequest(requestSchema.getAllVacancies),ctrl.getVacancies)
    .put(ValidationRequest(requestSchema.updateVacancy),auth,ctrl.updateVacancy)
    .delete(ValidationRequest(requestSchema.deleteVacancy),ctrl.deleteVacancy)

router
    .route("/vacancy")
    .get(ValidationRequest(requestSchema.getVacancy),ctrl.getVacancy);

export default router;