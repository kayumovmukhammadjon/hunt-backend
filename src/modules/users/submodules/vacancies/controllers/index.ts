import { Request,Response } from "express";
import VacancyService from "../services/index";

class ResumeController {
    async createVacancy(req:Request,res:Response){
        try{
            const {
                code,
                message,
                data:result
            } = await VacancyService.createVacancy(req.body);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Vacancy controller [createVacancy] error: ${error}`);
        }
    }

    async getVacancies(req:Request,res:Response){
        try{
            const query:any = req.query;

            console.log(query);

            const {
                code,
                message,
                data:result
            } = await VacancyService.getVacancies(query);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Vacancy controller [getVacancies] error: ${error}`);
        }
    }

    async getVacancy(req:Request,res:Response){
        try{
            const query:any= req.query;
            const {
                code,
                message,
                data:result
            } = await VacancyService.getVacancy(query);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Vacancy controller [getVacancy] error: ${error}`);
        }
    }

    async updateVacancy(req:Request,res:Response){
        try{
            const {
                code,
                message,
                data: result,
              } = await VacancyService.updateVacancy(req.body);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Vacancy controller [updateVacancy] error: ${error}`);
        }
    }

    async deleteVacancy(req:Request,res:Response){
        try{
            const query:any = req.query
            const {
                code,
                message,
                data: result,
              } = await VacancyService.deleteVacancy(query);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Vacancy controller [deleteVacancy] error: ${error}`);
        }
    }
}

export default new ResumeController();