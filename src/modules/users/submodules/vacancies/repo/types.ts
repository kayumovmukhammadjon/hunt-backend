import {IUser} from "../../../../../domain/entities/User";
import { ICompany } from "../../../../../domain/entities/Company";
import { IPosition } from "../../../../../domain/entities/Position";
import { ICurrency } from "../../../../../domain/entities/Currency";
import {IRegion} from "../../../../../domain/entities/Region";
import { IVacancy } from "../../../../../domain/entities/Vacancy";
import { IFile } from "../../../../../domain/entities/File";

export enum Status  {
    accepted = "eccepted",
    rejected = "rejected",
    pending = "pending",
}

export enum availability {
    day = "day",
    night = "night"
}

export enum Type {
    office="office",
    remote="remote"
}

export enum SalaryType {
    day = "day",
    week = "week",
    month = "month",
    year = "year"
}

export enum PaymentStatus {
    unpaid = "unpaid",
    paid = "paid",
}

export type TCreateVacancy= {
    index:number;
    user:IUser;
    company:ICompany;
    position:IPosition;
    availability:availability;
    type:Type;
    workHours:{
        startTime:string;
        endTime:string;
    },
    salary:{
        price:string;
        currency:ICurrency,  
        type:SalaryType;
    },
    experience:string;
    skills:string[];
    description:string;
    region:IRegion;
    language:string;
    contact:{
        username:string;
        phone:string;
        email:string;
    }
}

export type TGetVacancById = {
    vacancyId:string;
}

export type TGetVacancies = {
    skip:string;
    limit:string;
    position?:IPosition;
    availability?:string;
    type?:string;
    region?:IRegion;
    experience?:string;
}

export type TUpdateVacancy = {
    vacancy:IVacancy
    user:IUser;
    company:ICompany;
    position:IPosition;
    availability:availability;
    type:Type;
    workHours:{
        startTime:string;
        endTime:string;
    },
    salary:{
        price:string;
        currency:ICurrency,  
        type:SalaryType;
    },
    experience:string;
    skills:string[];
    description:string;
    region:IRegion;
    language:string;
    contact:{
        username:string;
        phone:string;
        email:string;
    }
}

export type TDeleteVacancy =  {
    vacancy:IVacancy;
}