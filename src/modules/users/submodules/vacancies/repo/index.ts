import Vacancy from "../../../../../database/models/Vacancy";
import { TCreateVacancy,TDeleteVacancy,TGetVacancById,TGetVacancies,TUpdateVacancy } from "./types";
import logger from "../../../../../shared/logger";
import { configurations } from "../../../../../config";
import mongoose from "mongoose";

class VacancyRepo {
    async createVacancy(data:TCreateVacancy){
        try{
            const newVacancy = new Vacancy(data)

            await newVacancy.save();

            return newVacancy;
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "vacancy.repo",
                  method: "POST",
                  methodName: "createVacancy",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/vacancies`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [vacancy.repo] createVacancy"
              );
              console.log(`ERROR: [vacancy.repo] createVacancy: ${error}`);
              throw error;
        }
    }

    async getVacancies(data:TGetVacancies){
        try{
            let {region,position,experience,type,availability,limit,skip} = data

            let matchQuery:any = {isDeleted:false};

            if(region){
              matchQuery.region = new mongoose.Types.ObjectId(region.id);
            }
            
            if(position){
              matchQuery.position =  new mongoose.Types.ObjectId(position.id);;
            }

            if(experience)  {
              matchQuery.experience = experience;
            }
            
            if(type){
              matchQuery.type = type;
            }

            if(availability){
              matchQuery.availability = availability;
            }

            return await Vacancy.aggregate([
              {
                $match:matchQuery
              },
              {
                $sort: { createdAt: -1 },
              },
              {
                $lookup: {
                  from: "positions",
                  localField: "position",
                  foreignField: "_id",
                  as: "position",
                },
              },
              {
                $unwind: "$position",
              },
              {
                $lookup: {
                  from: "regions",
                  localField: "region",
                  foreignField: "_id",
                  as: "region",
                },
              },
              {
                $unwind: "$region",
              },
              {
                $limit:+limit
              },
              {
                $skip:+skip
              }
            ])
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "vacancy.repo",
                  method: "GET",
                  methodName: "getVacancies",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/vacancies`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [vacancy.repo] getVacancies"
              );
            console.log(`ERROR: [vacancy.repo] getVacancies: ${error}`);
            throw error;
        }
    }

    async getVacancyById(data:TGetVacancById){
        try{
            return await Vacancy.findOne({
                _id:data.vacancyId,
                isDeleted:false
            })
            .populate("position")
            .populate({
              path:"position",
              populate:{
                path:"file"
              }
            })
            .populate("company")
            .populate({
              path:"company",
              populate:{
                path:"file"
              }
            })
            .populate("region")
            .populate("salary.currency")
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "vacancy.repo",
                  method: "GET",
                  methodName: "getVacancyById",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/vacancies`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [vacancy.repo] getVacancyById"
              );
            console.log(`ERROR: [vacancy.repo] getVacancyById: ${error}`);
            throw error;
        }
    }

    async updateVacancy(data:TUpdateVacancy){
        try{
            return await Vacancy.findByIdAndUpdate({
                isDeleted:false,
                _id:data.vacancy.id
            },{
                ...data
            },{
                new:true
            })
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "resume.repo",
                  method: "PUT",
                  methodName: "updateVacancy",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/vacancies`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [vacancy.repo] updateVacancy"
              );
            console.log(`ERROR: [vacancy.repo] updateVacancy: ${error}`);
            throw error;
        }
    }

    async deleteVacancy(data:TDeleteVacancy){
        try{
            return await Vacancy.findByIdAndUpdate({
                isDeleted:false,
                _id:data.vacancy.id
            },{
              isDeleted:true
            })
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "vacancy.repo",
                  method: "DELETE",
                  methodName: "deleteVacancy",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/vacancies`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [vacancy.repo] deleteVacancy"
              );
            console.log(`ERROR: [vacancy.repo] deleteVacancy: ${error}`);
            throw error;
        }
    }
}

export default VacancyRepo;