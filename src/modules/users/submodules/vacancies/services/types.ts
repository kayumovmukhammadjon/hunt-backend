import {IUser} from "../../../../../domain/entities/User";
import { ICompany } from "../../../../../domain/entities/Company";
import { IPosition } from "../../../../../domain/entities/Position";
import { ICurrency } from "../../../../../domain/entities/Currency";
import {IRegion} from "../../../../../domain/entities/Region";
import { IVacancy } from "../../../../../domain/entities/Vacancy";
import { IFile } from "../../../../../domain/entities/File";

export enum Status  {
    accepted = "eccepted",
    rejected = "rejected",
    pending = "pending",
}

export enum availability {
    day = "day",
    night = "night"
}

export enum Type {
    office="office",
    remote="remote"
}

export enum SalaryType {
    day = "day",
    week = "week",
    month = "month",
    year = "year"
}

export enum PaymentStatus {
    unpaid = "unpaid",
    paid = "paid",
}

export type TCreateVacancy= {
    index:number;
    user:IUser;
    userId:string;
    company:ICompany;
    companyId:string;
    position:IPosition;
    positionId:string;
    availability:availability;
    type:Type;
    workHours:{
        startTime:string;
        endTime:string;
    },
    salary:{
        price:string;
        currency:ICurrency;
        currencyId:string;
        type:SalaryType;
    },
    experience:string;
    skills:string[];
    description:string;
    region:IRegion;
    regionId:string;
    language:string;
    contact:{
        username:string;
        phone:string;
        email:string;
    }
}

export type TGetVacancyById = {
    vacancyId:string;
}

export type TGetVacancy = {
    vacancyId:string;
    vacancy:IVacancy;
}

export type TGetVacancies = {
    skip:string;
    limit:string;
    positionId?:string;
    position?:IPosition;
    availability?:string;
    type?:string;
    regionId?:string;
    region?:IRegion;
    experience?:string;
}

export type TUpdateVacancy = {
    vacancy:IVacancy
    vacancyId:string;
    user:IUser;
    userId:string;
    company:ICompany;
    companyId:string;
    position:IPosition;
    positionId:string;
    availability:availability;
    type:Type;
    workHours:{
        startTime:string;
        endTime:string;
    },
    salary:{
        price:string;
        currency:ICurrency; 
        currencyId:string;
        type:SalaryType;
    },
    experience:string;
    skills:string[];
    description:string;
    region:IRegion;
    regionId:string;
    language:string;
    contact:{
        username:string;
        phone:string;
        email:string;
    }
}

export type TDeleteVacancy =  {
    vacancy:IVacancy;
    vacancyId:string;
}