import {TCreateVacancy,TDeleteVacancy,TGetVacancyById,TGetVacancy,TGetVacancies,TUpdateVacancy } from "./types";
import logger from "../../../../../shared/logger";
import { configurations } from "../../../../../config";
import UserService from "../../../services/index";
import {ok,fail} from "../../../../../utils/response";
import RegionService from "../../../../region/service/service";
import PositionService from "../../positions/services/index";
import CompanyService from "../../companies/services/index";
import CurrencyService from "../../../../currency/services/index";
import VacancyRepo from "../repo";



const repo = new VacancyRepo();

class VacancyService {
    async createVacancy(data:TCreateVacancy){
      try{
        const user = await UserService.getUserById({
          userId:data.userId
        })

        if(!user){
          return fail(404,"User Not Found!")
        }

        data.user = user;


        const region = await RegionService.getRegionById({
          id:data.regionId
        })

        if(!region){
          return fail(404,"Region Not Found");
        }

        data.region = region

        const position = await PositionService.getPositionById({
          positionId:data.positionId
        })

        if(!position){
          return fail(404,"Position Not Found");
        }

        data.position = position

        const company = await CompanyService.getCompanyById({
          companyId:data.companyId
        })

        if(!company){
          return fail(404,"Company Not Found");
        }

        data.company = company;


        const currency = await CurrencyService.getCurrencyById({
          currencyId:data.salary.currencyId
        });

        if(!currency){
          return fail(404,"Currency Not Found");
        }


        data.salary.currency = currency;

       const newVacancy = await repo.createVacancy(data)

        return ok(newVacancy);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "vacancy.service",
            method: "POST",
            methodName: "createVacancy",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: data,
              query: null,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [vacancy.service] createVacancy"
        );
        console.log(`ERROR: [vacancy.service] createVacancy: ${error}`);
        throw error;
      }
    }

    async updateVacancy(data:TUpdateVacancy){
      try{
        const vacancy = await repo.getVacancyById({
          vacancyId:data.vacancyId
        })

        if(!vacancy){
          return fail(404,"Vacancy Not Found!")
        }

        data.vacancy = vacancy;

        const user = await UserService.getUserById({
          userId:data.userId
        })

        if(!user){
          return fail(404,"User Not Found!")
        }

        data.user = user;


        const region = await RegionService.getRegionById({
          id:data.regionId
        })

        if(!region){
          return fail(404,"Region Not Found");
        }

        data.region = region

        const position = await PositionService.getPositionById({
          positionId:data.positionId
        })

        if(!position){
          return fail(404,"Position Not Found");
        }

        data.position = position

        const company = await CompanyService.getCompanyById({
          companyId:data.companyId
        })

        if(!company){
          return fail(404,"Company Not Found");
        }

        data.company = company;


        const currency = await CurrencyService.getCurrencyById({
          currencyId:data.salary.currencyId
        });

        if(!currency){
          return fail(404,"Currency Not Found");
        }


        data.salary.currency = currency;

       const updatedVacancy = await repo.updateVacancy(data);

        return ok(updatedVacancy);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "vacancy.service",
            method: "PUT",
            methodName: "updateVacancy",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [vacancy.service] updateVacancy"
        );
        console.log(`ERROR: [vacancy.service] updateVacancy: ${error}`);
        throw error;
      }
    }

    async getVacancies(data:TGetVacancies){
      try{

        if(data.regionId){
          let region = await RegionService.getRegionById({
            id:data.regionId
          });

          if(!region){
            return fail(404,"Region Not Found")
          }

          data.region = region;
        }

        if(data.positionId){
          let position = await PositionService.getPositionById({
            positionId:data.positionId
          })

          if(!position){
            return fail(404,"Position Not Found")
          }

          data.position = position;
        }

        const vacancies = await repo.getVacancies(data);

        return ok(vacancies);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "vacancy.service",
            method: "GET",
            methodName: "getVacancies",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [vacancy.service] getVacancies"
        );
        console.log(`ERROR: [vacancy.service] getVacancies: ${error}`);
        throw error;
      }
    }

    async getVacancy(data:TGetVacancy){
      try{
        const vacany = await repo.getVacancyById({
          vacancyId:data.vacancyId
        })

        if(!vacany){
          return fail(404,"Vacancy Not Found!")
        }
      
        return ok(vacany);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "vacancy.service",
            method: "GET",
            methodName: "getVacancy",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [vacancy.service] getVacancy"
        );
        console.log(`ERROR: [vacancy.service] getVacancy: ${error}`);
        throw error;
      }
    }

    async getVacancyById(data:TGetVacancyById){
      try{
        return await repo.getVacancyById({
          vacancyId:data.vacancyId
        })
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "vacancy.service",
            method: "GET",
            methodName: "getVacancyById",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [vacancy.service] getVacancyById"
        );
        console.log(`ERROR: [vacancy.service] getVacancyById: ${error}`);
        throw error;
      }
    }

    async deleteVacancy(data:TDeleteVacancy){
      try{
        const vacancy = await repo.getVacancyById({
          vacancyId:data.vacancyId
        })

        if(!vacancy){
          return fail(404,"Resume Not Found!")
        }

        data.vacancy = vacancy;

        const deletedVacancy = await repo.deleteVacancy(data);
      
        return ok(deletedVacancy);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "vacancy.service",
            method: "DELETE",
            methodName: "deleteVacancy",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [vacancy.service] deleteVacancy"
        );
        console.log(`ERROR: [vacancy.service] deleteVacancy: ${error}`);
        throw error;
      }
    }
}

export default new VacancyService();