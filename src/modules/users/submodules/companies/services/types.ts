import { ICompany } from "../../../../../domain/entities/Company";
import { IFile } from "../../../../../domain/entities/File"
import { IUser } from "../../../../../domain/entities/User";

export type TCreateCompany= {
    user:IUser;
    userId:string;
    name:string;
    file:IFile;
    fileId:string;
    phoneNumber:string;
}

export type TGetCompany = {
    companyId:string;
    company:ICompany
}
export type TGetCompanyById = {
    companyId:string;
}

export type TGetCompanies = {
    skip:string;
    limit:string;
}

export type TGetCompaniesByUserId = {
    user:IUser;
    userId:string;
    skip:string;
    limit:string;
} 

export type TUpdateCompany = {
    company:ICompany;
    companyId:string;
    user:IUser;
    userId:string;
    name:string;
    file:IFile;
    fileId:string;
    phoneNumber:string;
}

export type TDeleteCompany =  {
    company:ICompany;
    companyId:string;
}