import { TCreateCompany,TDeleteCompany,TGetCompanies,TGetCompaniesByUserId,TGetCompanyById,TUpdateCompany,TGetCompany } from "./types";
import logger from "../../../../../shared/logger";
import { configurations } from "../../../../../config";
import CompanyRepo from "../repo";
import UserService from "../../../services/index";
import FileService from "../../../../files/service/index";
import {ok,fail} from "../../../../../utils/response";


const repo = new CompanyRepo();

class CompanyService {
    async createCompany(data:TCreateCompany){
      try{
        const user = await UserService.getUserById({
          userId:data.userId
        })

        if(!user){
          return fail(404,"User Not Found!")
        }

        data.user = user;
      
        const file = await FileService.getFileById(data.fileId);

        if(!file){
          return fail(404,"File Not Found");
        }

        data.file = file

        const newCompany = await repo.createCompany(data);

        return ok(newCompany);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "company.service",
            method: "POST",
            methodName: "createCompany",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: data,
              query: null,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [company.service] createCompany"
        );
        console.log(`ERROR: [company.service] createCompany: ${error}`);
        throw error;
      }
    }

    async updateCompany(data:TUpdateCompany){
      try{
        const company = await repo.getCompanyById({
          companyId:data.companyId
        })

        if(!company){
          return fail(404,"Company Not Found!")
        }

        data.company = company;

        const user = await UserService.getUserById({
          userId:data.userId
        })

        if(!user){
          return fail(404,"User Not Found!")
        }

        data.user = user;
      
        const file = await FileService.getFileById(data.fileId);

        if(!file){
          return fail(404,"File Not Found");
        }

        data.file = file

        const newCompany = await repo.updateCompany(data);

        return ok(newCompany);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "company.service",
            method: "PUT",
            methodName: "updateCompany",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: data,
              query: null,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [company.service] updateCompany"
        );
        console.log(`ERROR: [company.service] updateCompany: ${error}`);
        throw error;
      }
    }

    async getCompanies(data:TGetCompanies){
      try{
        const companies = await repo.getCompanies(data);

        return ok(companies);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "company.service",
            method: "GET",
            methodName: "getCompanies",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [company.service] getCompanies"
        );
        console.log(`ERROR: [company.service] getCompanies: ${error}`);
        throw error;
      }
    }

    async getCompanyByUserId(data:TGetCompaniesByUserId){
      try{
        const user = await UserService.getUserById({
          userId:data.userId
        })

        if(!user){
          return fail(404,"User Not Found!")
        }

        data.user = user;

        const newCompany = await repo.getCompaniesByUserId(data);

        return ok(newCompany);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "company.service",
            method: "GET",
            methodName: "getCompanyByUserId",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [company.service] getCompanyByUserId"
        );
        console.log(`ERROR: [company.service] getCompanyByUserId: ${error}`);
        throw error;
      }
    }

    async getCompany(data:TGetCompany){
      try{
        const company = await repo.getCompanyById({
          companyId:data.companyId
        })

        if(!company){
          return fail(404,"Company Not Found!")
        }

        data.company = company;
      

        return ok(company);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "company.service",
            method: "GET",
            methodName: "getCompany",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [company.service] getCompany"
        );
        console.log(`ERROR: [company.service] getCompany: ${error}`);
        throw error;
      }
    }

    async getCompanyById(data:TGetCompanyById){
      try{
        return await repo.getCompanyById({
          companyId:data.companyId
        })
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "company.service",
            method: "GET",
            methodName: "getCompanyById",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [company.service] getCompanyById"
        );
        console.log(`ERROR: [company.service] getCompany: ${error}`);
        throw error;
      }
    }

    async deleteCompany(data:TDeleteCompany){
      try{
        const company = await repo.getCompanyById({
          companyId:data.companyId
        })

        if(!company){
          return fail(404,"Company Not Found!")
        }

        data.company = company;

        const deletedCompany = await repo.deleteCompany(data);
      

        return ok(deletedCompany);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "company.service",
            method: "GET",
            methodName: "createCompany",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: data,
              query: null,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [company.service] createCompany"
        );
        console.log(`ERROR: [company.service] createCompany: ${error}`);
        throw error;
      }
    }
}

export default new CompanyService();