import { ICompany } from "../../../../../domain/entities/Company";
import { IFile } from "../../../../../domain/entities/File"
import { IUser } from "../../../../../domain/entities/User";

export type TCreateCompany= {
    name:string;
    file:IFile;
    phoneNumber:string;
    user:IUser;
}

export type TGetCompanyById = {
    companyId:string;
}

export type TGetCompanies = {
    skip:string;
    limit:string;
}

export type TGetCompaniesByUserId = {
    user:IUser;
    skip:string;
    limit:string;
} 

export type TUpdateCompany = {
    company:ICompany;
    name:string;
    file:IFile;
    phoneNumber:string;
}

export type TDeleteCompany =  {
    company:ICompany;
}