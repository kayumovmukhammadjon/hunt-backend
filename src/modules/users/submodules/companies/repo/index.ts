import Company from "../../../../../database/models/Company";
import { TCreateCompany,TDeleteCompany,TGetCompanies,TGetCompaniesByUserId,TGetCompanyById,TUpdateCompany } from "./types";
import logger from "../../../../../shared/logger";
import { configurations } from "../../../../../config";

class CompanyRepo {
    async createCompany(data:TCreateCompany){
        try{
            const newCompany = new Company(data)

            await newCompany.save();

            return newCompany;
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "company.repo",
                  method: "POST",
                  methodName: "createCompany",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/company`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [company.repo] createCompany"
              );
              console.log(`ERROR: [company.repo] createCompany: ${error}`);
              throw error;
        }
    }

    async getCompanies(data:TGetCompanies){
        try{
            return await Company.find({
                isDeleted:false
            })
            .populate("file")
            .skip(+data.skip).limit(+data.limit)
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "company.repo",
                  method: "GET",
                  methodName: "getCompanies",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/company`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [company.repo] getCompanies"
              );
            console.log(`ERROR: [company.repo] getCompanies: ${error}`);
            throw error;
        }
    }

    async getCompanyById(data:TGetCompanyById){
        try{
            return await Company.findOne({
                _id:data.companyId,
                isDeleted:false
            })
            .populate("file")
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "company.repo",
                  method: "GET",
                  methodName: "getCompanyById",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/company`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [company.repo] getCompanyById"
              );
            console.log(`ERROR: [company.repo] getCompanyById: ${error}`);
            throw error;
        }
    }

    async getCompaniesByUserId(data:TGetCompaniesByUserId){
        try{
            return await Company.find({
                isDeleted:false,
                user:data.user.id
            })
            .populate("file")
            .skip(+data.skip).limit(+data.limit)
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "company.repo",
                  method: "GET",
                  methodName: "getCompaniesByUserId",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/company`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [company.repo] getCompaniesByUserId"
              );
            console.log(`ERROR: [company.repo] getCompaniesByUserId: ${error}`);
            throw error;
        }
    }

    async updateCompany(data:TUpdateCompany){
        try{
            return await Company.findByIdAndUpdate({
                isDeleted:false,
                _id:data.company.id
            },{
                ...data
            },{
                new:true
            })
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "company.repo",
                  method: "PUT",
                  methodName: "updateCompany",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/company`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [company.repo] updateCompany"
              );
            console.log(`ERROR: [company.repo] updateCompany: ${error}`);
            throw error;
        }
    }

    async deleteCompany(data:TDeleteCompany){
        try{
            return await Company.findByIdAndUpdate({
                isDeleted:false,
                _id:data.company.id
            },{
               isDeleted:true
            })
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "company.repo",
                  method: "DELETE",
                  methodName: "deleteCompany",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/company`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [company.repo] deleteCompany"
              );
            console.log(`ERROR: [company.repo] deleteCompany: ${error}`);
            throw error;
        }
    }
}

export default CompanyRepo;