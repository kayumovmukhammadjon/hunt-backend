import { Request,Response } from "express";
import CompanyService from "../services/index";

class CompanyController {
    async createCompany(req:Request,res:Response){
        try{
            const {
                code,
                message,
                data:result
            } = await CompanyService.createCompany(req.body);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Company controller [createCompany] error: ${error}`);
        }
    }

    async getCompanies(req:Request,res:Response){
        try{
            const query:any = req.query;
            const {
                code,
                message,
                data:result
            } = await CompanyService.getCompanies(query);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Company controller [getCompanies] error: ${error}`);
        }
    }

    async getCompany(req:Request,res:Response){
        try{
            const query:any= req.query;
            const {
                code,
                message,
                data:result
            } = await CompanyService.getCompany(query);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Company controller [getCompany] error: ${error}`);
        }
    }

    async getCompaniesByUserId(req:Request,res:Response){
        try{
            const query:any= req.query;
            const {
                code,
                message,
                data:result
            } = await CompanyService.getCompanyByUserId(query);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Company controller [getCompaniesByUserId] error: ${error}`);
        }
    }


    async updateCompany(req:Request,res:Response){
        try{
            const {
                code,
                message,
                data: result,
              } = await CompanyService.updateCompany(req.body);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Company controller [updateCompany] error: ${error}`);
        }
    }

    async deleteCompany(req:Request,res:Response){
        try{
            const query:any = req.query
            const {
                code,
                message,
                data: result,
              } = await CompanyService.deleteCompany(query);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Company controller [deleteCompany] error: ${error}`);
        }
    }
}

export default new CompanyController();