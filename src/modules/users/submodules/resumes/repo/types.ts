import { IResume } from "../../../../../domain/entities/Resume";
import {IFile} from "../../../../../domain/entities/File";
import { IUser } from "../../../../../domain/entities/User";
import { IRegion } from "../../../../../domain/entities/Region";
import { IPosition } from "../../../../../domain/entities/Position";

export type TCreateResume= {
    index:number;
    user:IUser;
    file:IFile;
    region:IRegion;
    position:IPosition;
    language:string;
    contact:{
        username:string;
        phone:string;
        email:string;
    }
}

export type TGetResumeById = {
    resumeId:string;
}

export type TGetResumes = {
    skip:string;
    limit:string;
    position?:IPosition;
    region?:IRegion;
}


export type TUpdateResume = {
    resume:IResume
    user:IUser;
    file:IFile;
    region:IRegion;
    position:IPosition;
    language:string;
    contact:{
        username:string;
        phone:string;
        email:string;
    }
}

export type TDeleteResume =  {
    resume:IResume
}