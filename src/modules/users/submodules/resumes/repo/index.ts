import Resume from "../../../../../database/models/Resume";
import { TCreateResume,TDeleteResume,TGetResumeById,TGetResumes,TUpdateResume } from "./types";
import logger from "../../../../../shared/logger";
import { configurations } from "../../../../../config";
import mongoose from "mongoose";

class ResumeRepo {
    async createResume(data:TCreateResume){
        try{
            const newResume = new Resume(data)

            await newResume.save();

            return newResume;
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "resume.repo",
                  method: "POST",
                  methodName: "createResume",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/resumes`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [resume.repo] createResume"
              );
              console.log(`ERROR: [resume.repo] createResume: ${error}`);
              throw error;
        }
    }

    async getResumes(data:TGetResumes){
        try{
          let {region,position,limit,skip} = data

          let matchQuery:any = {isDeleted:false};

          if(region){
            matchQuery.region = new mongoose.Types.ObjectId(region.id);
          }
          
          if(position){
            matchQuery.position =  new mongoose.Types.ObjectId(position.id);;
          }


          return await Resume.aggregate([
            {
              $match:matchQuery
            },
            {
              $sort: { createdAt: -1 },
            },
            {
              $lookup: {
                from: "positions",
                localField: "position",
                foreignField: "_id",
                as: "position",
              },
            },
            {
              $unwind: "$position",
            },
            {
              $lookup: {
                from: "regions",
                localField: "region",
                foreignField: "_id",
                as: "region",
              },
            },
            {
              $unwind: "$region",
            },
            {
              $limit:+limit
            },
            {
              $skip:+skip
            }
          ])
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "resume.repo",
                  method: "GET",
                  methodName: "getResumes",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/resumes`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [resume.repo] getResumes"
              );
            console.log(`ERROR: [resume.repo] getResumes: ${error}`);
            throw error;
        }
    }

    async getResumeById(data:TGetResumeById){
        try{
            return await Resume.findOne({
                _id:data.resumeId,
                isDeleted:false
            })
            .populate("user")
            .populate("region")
            .populate("position")
            .populate({
              path:"position",
              populate:{
                path:"file"
              }
            })
            .populate("file")
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "resume.repo",
                  method: "GET",
                  methodName: "getResumeById",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/resumes/resume`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [resume.repo] getResumeById"
              );
            console.log(`ERROR: [resume.repo] getResumeById: ${error}`);
            throw error;
        }
    }



    async updateResume(data:TUpdateResume){
        try{
            return await Resume.findByIdAndUpdate({
                isDeleted:false,
                _id:data.resume.id
            },{
                ...data
            },{
                new:true
            })
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "resume.repo",
                  method: "PUT",
                  methodName: "updateResume",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/resumes`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [resume.repo] updateResume"
              );
            console.log(`ERROR: [resume.repo] updateResume: ${error}`);
            throw error;
        }
    }

    async deleteResume(data:TDeleteResume){
        try{
            return await Resume.findByIdAndUpdate({
                isDeleted:false,
                _id:data.resume.id
            },{
              isDeleted:true
            })
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "resume.repo",
                  method: "DELETE",
                  methodName: "deleteResume",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/resumes`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [resume.repo] deleteResume"
              );
            console.log(`ERROR: [resume.repo] deleteResume: ${error}`);
            throw error;
        }
    }
}

export default ResumeRepo;