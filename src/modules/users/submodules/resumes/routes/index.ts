import { Router } from "express";
import ValidationRequest from "../../../../../middlewares/ValidationRequest";
import requestSchema from "../validations/index";
import auth from "../../../../../middlewares/Auth";

import UserController from "../controllers/index";


const router = Router({mergeParams:true});
const ctrl = UserController;

router
    .route("/")
    .post(ValidationRequest(requestSchema.createResume),auth,ctrl.createResume)
    .get(ValidationRequest(requestSchema.getAllResumes),ctrl.getResumes)
    .put(ValidationRequest(requestSchema.updateResume),ctrl.updateResume)
    .delete(ValidationRequest(requestSchema.deleteResume),ctrl.deleteResume)

router
    .route("/resume")
    .get(ValidationRequest(requestSchema.getResume),auth,ctrl.getResume);

export default router;