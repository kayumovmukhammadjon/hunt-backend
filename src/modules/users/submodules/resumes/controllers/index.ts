import { Request,Response } from "express";
import ResumeService from "../services/index";

class ResumeController {
    async createResume(req:Request,res:Response){
        try{
            const {
                code,
                message,
                data:result
            } = await ResumeService.createResume(req.body);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Resume controller [createPosition] error: ${error}`);
        }
    }

    async getResumes(req:Request,res:Response){
        try{
            const query:any = req.query;

            const {
                code,
                message,
                data:result
            } = await ResumeService.getPesumes(query);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Resume controller [getCompanies] error: ${error}`);
        }
    }

    async getResume(req:Request,res:Response){
        try{
            const query:any= req.query;
            const {
                code,
                message,
                data:result
            } = await ResumeService.getResume(query);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Resume controller [getCompany] error: ${error}`);
        }
    }

    async updateResume(req:Request,res:Response){
        try{
            const {
                code,
                message,
                data: result,
              } = await ResumeService.updateResume(req.body);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Resume controller [updateCompany] error: ${error}`);
        }
    }

    async deleteResume(req:Request,res:Response){
        try{
            const query:any = req.query
            const {
                code,
                message,
                data: result,
              } = await ResumeService.deleteResume(query);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Resume controller [deleteCompany] error: ${error}`);
        }
    }
}

export default new ResumeController();