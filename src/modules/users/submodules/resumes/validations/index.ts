import Joi from "joi";

interface PositionSchema {
    createResume:Joi.Schema<any>;
    updateResume:Joi.Schema<any>;
    deleteResume:Joi.Schema<any>
    getAllResumes:Joi.Schema<any>
    getResume:Joi.Schema<any>
}

const schema:PositionSchema = {
    createResume:Joi.object().keys({
        body:Joi.object().keys({
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            regionId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            positionId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            fileId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            language:Joi.string().required(),
            contact:Joi.object().keys({
                username:Joi.string().optional(),
                phone:Joi.string().optional(),
                email:Joi.string().optional(),
            }),
        }).required()
    }),
    updateResume:Joi.object().keys({
        body:Joi.object().keys({
            resumeId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            regionId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            positionId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            fileId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            language:Joi.string().required(),
            contact:Joi.object().keys({
                username:Joi.string().optional(),
                phone:Joi.string().optional(),
                email:Joi.string().optional(),
            }),
        }).required()
    }),
    deleteResume:Joi.object().keys({
        query:Joi.object().keys({
            resumeId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
        }).required()
    }),
    getAllResumes:Joi.object().keys({
        query:Joi.object().keys({
            limit: Joi.string().required(),
            skip:Joi.string().required(),
            regionId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .optional(),
            positionId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .optional(),
        }).required()
    }),
    getResume:Joi.object().keys({
        query:Joi.object().keys({
            resumeId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
        }).required()
    }),
}

export default schema;