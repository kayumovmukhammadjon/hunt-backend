import {TCreateResume,TDeleteResume,TGetResumeById,TGetResumes,TUpdateResume,TGetResume } from "./types";
import logger from "../../../../../shared/logger";
import { configurations } from "../../../../../config";
import UserService from "../../../services/index";
import FileService from "../../../../files/service/index";
import {ok,fail} from "../../../../../utils/response";
import RegionService from "../../../../region/service/service";
import PositionService from "../../positions/services/index";
import ResumeRepo from "../repo";


const repo = new ResumeRepo();

class ResumeService {
    async createResume(data:TCreateResume){
      try{
        const user = await UserService.getUserById({
          userId:data.userId
        })

        if(!user){
          return fail(404,"User Not Found!")
        }

        data.user = user;
      
        const file = await FileService.getFileById(data.fileId);

        if(!file){
          return fail(404,"File Not Found");
        }

        data.file = file

        const region = await RegionService.getRegionById({
          id:data.regionId
        })

        if(!region){
          return fail(404,"Region Not Found");
        }

        data.region = region

        const position = await PositionService.getPositionById({
          positionId:data.positionId
        })

        if(!position){
          return fail(404,"Position Not Found");
        }

        data.position = position

        const newResume = await repo.createResume(data);

        return ok(newResume);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "resume.service",
            method: "POST",
            methodName: "createResume",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: data,
              query: null,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [resume.service] createResume"
        );
        console.log(`ERROR: [resume.service] createResume: ${error}`);
        throw error;
      }
    }

    async updateResume(data:TUpdateResume){
      try{
        const resume = await repo.getResumeById({
          resumeId:data.resumeId
        });

        if(!resume){
          return fail(404,"Resume Not Found!")
        }

        data.resume = resume;

        const user = await UserService.getUserById({
          userId:data.userId
        })

        if(!user){
          return fail(404,"User Not Found!")
        }

        data.user = user;
      
        const file = await FileService.getFileById(data.fileId);

        if(!file){
          return fail(404,"File Not Found");
        }

        data.file = file

        const region = await RegionService.getRegionById({
          id:data.regionId
        })

        if(!region){
          return fail(404,"Region Not Found");
        }

        data.region = region

        const position = await PositionService.getPositionById({
          positionId:data.positionId
        })

        if(!position){
          return fail(404,"Position Not Found");
        }

        data.position = position

        const updatedResume = await repo.updateResume(data);

        return ok(updatedResume);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "resume.service",
            method: "PUT",
            methodName: "updateResume",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [resume.service] updateCompany"
        );
        console.log(`ERROR: [resume.service] updateCompany: ${error}`);
        throw error;
      }
    }

    async getPesumes(data:TGetResumes){
      try{
        if(data.regionId){
          let region = await RegionService.getRegionById({
            id:data.regionId
          });

          if(!region){
            return fail(404,"Region Not Found")
          }

          data.region = region;
        }

        if(data.positionId){
          let position = await PositionService.getPositionById({
            positionId:data.positionId
          })

          if(!position){
            return fail(404,"Position Not Found")
          }

          data.position = position;
        }

        const resumes = await repo.getResumes(data);

        return ok(resumes);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "resume.service",
            method: "GET",
            methodName: "getPesumes",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [resume.service] getPesumes"
        );
        console.log(`ERROR: [resume.service] getPesumes: ${error}`);
        throw error;
      }
    }

    async getResume(data:TGetResume){
      try{
        const resume = await repo.getResumeById({
          resumeId:data.resumeId
        })

        if(!resume){
          return fail(404,"Resume Not Found!")
        }
      

        return ok(resume);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "resume.service",
            method: "GET",
            methodName: "getResume",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [resume.service] getResume"
        );
        console.log(`ERROR: [resume.service] getResume: ${error}`);
        throw error;
      }
    }

    async getResumeById(data:TGetResumeById){
      try{
        return await repo.getResumeById({
          resumeId:data.resumeId
        })
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "resume.service",
            method: "GET",
            methodName: "getResumeById",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [resume.service] getResumeById"
        );
        console.log(`ERROR: [resume.service] getResumeById: ${error}`);
        throw error;
      }
    }

    async deleteResume(data:TDeleteResume){
      try{
        const resume = await repo.getResumeById({
          resumeId:data.resumeId
        })

        if(!resume){
          return fail(404,"Resume Not Found!")
        }

        data.resume = resume;

        const deletedResume = await repo.deleteResume(data);
      
        return ok(deletedResume);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "resume.service",
            method: "DELETE",
            methodName: "deletedResume",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [resume.service] deletedResume"
        );
        console.log(`ERROR: [resume.service] deletedResume: ${error}`);
        throw error;
      }
    }
}

export default new ResumeService();