import { IResume } from "../../../../../domain/entities/Resume";
import {IFile} from "../../../../../domain/entities/File";
import { IUser } from "../../../../../domain/entities/User";
import { IRegion } from "../../../../../domain/entities/Region";
import { IPosition } from "../../../../../domain/entities/Position";

export type TCreateResume= {
    index:number;
    user:IUser;
    userId:string;
    file:IFile;
    fileId:string;
    region:IRegion;
    regionId:string;
    position:IPosition;
    positionId:string;
    language:string;
    contact:{
        username:string;
        phone:string;
        email:string;
    }
}

export type TGetResumeById = {
    resumeId:string;
}

export type TGetResume = {
    resumeId:string;
    resume:IResume
}

export type TGetResumes = {
    skip:string;
    limit:string;
    positionId?:string;
    position?:IPosition;
    regionId?:string;
    region?:IRegion;
}


export type TUpdateResume = {
    resume:IResume
    resumeId:string;
    user:IUser;
    userId:string;
    file:IFile;
    fileId:string;
    region:IRegion;
    regionId:string;
    position:IPosition;
    positionId:string;
    language:string;
    contact:{
        username:string;
        phone:string;
        email:string;
    }
}

export type TDeleteResume =  {
    resume:IResume
    resumeId:string;
}