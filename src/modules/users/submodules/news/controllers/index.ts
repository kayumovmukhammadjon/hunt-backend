import { Request,Response } from "express";
import NewsService from "../services/index";

class NewsController {
    async createNews(req:Request,res:Response){
        try{
            const {
                code,
                message,
                data:result
            } = await NewsService.create(req.body);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`News controller [getAllNew] error: ${error}`);
        }
    }

    async getAllNews(req:Request,res:Response){
        try{
            const query:any = req.query;

            const {
                code,
                message,
                data:result
            } = await NewsService.getAllNews(query);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`News controller [getAllNews] error: ${error}`);
        }
    }


    async getNews(req:Request,res:Response){
        try{
            const query:any = req.query;

            const {
                code,
                message,
                data: result,
              } = await NewsService.getNews(query);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`News controller [getNews] error: ${error}`);
        }
    }

    async updateNews(req:Request,res:Response){
        try{
            const {
                code,
                message,
                data: result,
              } = await NewsService.updateNews(req.body);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`News controller [updateNews] error: ${error}`);
        }
    }

    async deleteNews(req:Request,res:Response){
        try{
            const query:any = req.query;

            const {
                code,
                message,
                data: result,
              } = await NewsService.deleteNews(query)
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Episode controller [deleteNews] error: ${error}`);
        }
    }
}

export default new NewsController();