import { IUser } from "../../../../../domain/entities/User";
import {IFile} from "../../../../../domain/entities/File";
import { INew } from "../../../../../domain/entities/New";

export type TCreateNews = {
    title:string;
    user:IUser;
    description:string;
    files:IFile[];
}


export type TGetNewsById = {
    newsId:string;
}

export type TUpdateNews = {
    news:INew;
    title:string;
    user:IUser;
    description:string;
    files:IFile[];
}

export type TDeleteNews = {
    news:INew;
}

export type TGetAllNews = {
    skip:string;
    limit:string;
}



