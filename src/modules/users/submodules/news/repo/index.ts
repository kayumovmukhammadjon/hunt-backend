import New from "../../../../../database/models/New";
import {TCreateNews,TDeleteNews,TGetAllNews,TGetNewsById,TUpdateNews} from "./types";

class NewsRepo {
    async create(data:TCreateNews){
        const newEpisode = new New(data)

        await newEpisode.save()

        return newEpisode;
    }

    async getNewsById(data:TGetNewsById){
        return await New.findOne({
            _id:data.newsId,
            isDeleted:false
        })
    }

    async getAllNews(data:TGetAllNews){
        return await New.find({isDeleted:false}).skip(+data.skip).limit(+data.limit);
    }

    async updateNews(data:TUpdateNews){
        return await New.findOneAndUpdate({
            _id:data.user.id,
            isDeleted:false
        },data,{new:true})
    } 

   

    async deleteNews(data:TDeleteNews){
        return await New.findOneAndUpdate({
            _id:data.news.id,
            isDeleted:false
        },{
            isDeleted:true
        })
    }
}

export default NewsRepo;