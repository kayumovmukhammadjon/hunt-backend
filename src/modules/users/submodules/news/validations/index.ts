import Joi from "joi";

interface UserSchema {
    getNewsById:Joi.Schema<any>;
    updateNews:Joi.Schema<any>;
    deleteNews:Joi.Schema<any>;
    getAllNews:Joi.Schema<any>;
    createNews:Joi.Schema<any>;
}

const schema:UserSchema = {
    createNews:Joi.object().keys({
        body:Joi.object().keys({
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            description:Joi.string().required(),
            title:Joi.string().required(),
            fileIds: Joi.array().items(Joi.string().pattern(/^[0-9a-fA-F]{24}$/)).required(),
        })
    }),
    getNewsById:Joi.object().keys({
        query:Joi.object().keys({
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
        }).required()
    }),
    getAllNews:Joi.object().keys({
        query:Joi.object().keys({
            skip:Joi.string().required(),
            limit:Joi.string().required()
        }).required()
    }),
    updateNews:Joi.object().keys({
        body:Joi.object().keys({
            newsId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            description:Joi.string().required(),
            title:Joi.string().required(),
            fileIds: Joi.array().items(Joi.string().pattern(/^[0-9a-fA-F]{24}$/)).required(),
        })
    }),
    deleteNews:Joi.object().keys({
        query:Joi.object().keys({
            newsId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
        }).required()
    })
}

export default schema;