import { TCreateNews,TDeleteNews,TGetAllNews,TGetNewsById,TUpdateNews} from "./types";
import { configurations } from "../../../../../config";
import logger from "../../../../../shared/logger";
import EpisodeRepo from "../repo";
import { fail,ok } from "../../../../../utils/response";
import FileService from "../../../../files/service/index";
import NewsRepo from "../repo";

const repo = new NewsRepo();

class NewsService {
    async create(data: TCreateNews) {
        try {
            const newUser = await repo.create(data);

            return ok(newUser);
        } catch (error:any) {
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "news.service",
                  method: "POST",
                  methodName: "create",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `clients`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [news.service] create"
              );
              console.log(`ERROR: [news.service] create: ${error}`);
              return fail(500,error);
        }
    }

    async getNewsById(data:TGetNewsById){
        try{
            return await repo.getNewsById({
                newsId:data.newsId
            });
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "news.service",
                  method: "GET",
                  methodName: "getNewsById",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `getNewsById`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [news.service] getNewsById"
              );
            console.log(`ERROR: [news.service] getNewsById: ${error}`);
        }
    }

    async getAllNews(data:TGetAllNews){
        try{
            const allNews =  await repo.getAllNews(data);

            return ok(allNews);
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "news.service",
                  method: "GET",
                  methodName: "getAllNews",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `getAllUsers`,
                  apiData: {
                    body: null,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [news.service] getAllNews"
              );
            console.log(`ERROR: [news.service] getAllNews: ${error}`);
            return fail(500,error);
        }
    }

    async getNews(data:TGetNewsById){
        try{
            const news = await repo.getNewsById({
                newsId:data.newsId
            });

            if(!news){
                return fail(404,"News Not Found!");
            }

            return ok(news);
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "users.service",
                  method: "GET",
                  methodName: "getNews",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `getNews`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [news.service] getNews"
              );
            console.log(`ERROR: [news.service] getNews: ${error}`);
            return fail(500, error);0
        }
    }

    async updateNews(data:TUpdateNews){
        try{
            const news = await repo.getNewsById({
                newsId:data.newsId
            })

            if(!news){
                return fail(404,"Episode Not Found!");
            }
            
            data.news = news;

            if(data.fileIds.length>0){
              let files = await FileService.getFilesByIds(data.fileIds);

              data.files = files
            }

            const updatedNews =  await repo.updateNews(data);

            return ok(updatedNews);
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "news.service",
                  method: "PUT",
                  methodName: "updateNews",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `updateNews`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [news.service] updateNews"
              );
            console.log(`ERROR: [news.service] updateEpisode: ${error}`);
            return fail(500,error);
        }
    }

    async deleteNews(data:TDeleteNews){
        try{
            const news = await repo.getNewsById({
                newsId:data.newsId
            });

            if(!news){
                return fail(404,"News Not Found");
            }

            data.news = news;

            const deletedNews =  await repo.deleteNews(data);

            return ok(deletedNews);
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "news.service",
                  method: "DELETE",
                  methodName: "deleteNews",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `deleteNews`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [news.service] deleteNews"
              );
            console.log(`ERROR: [news.service] deleteNews: ${error}`);
            return fail(500,error)
        }
    }
}


export default new NewsService();