import { IUser } from "../../../../../domain/entities/User";
import {IFile} from "../../../../../domain/entities/File";
import { INew } from "../../../../../domain/entities/New";

export type TCreateNews = {
    title:string;
    user:IUser;
    userId:string;
    description:string;
    files:IFile[];
    fileIds:string[];
}


export type TGetNewsById = {
    newsId:string;
}

export type TUpdateNews = {
    newsId:string;
    news:INew;
    title:string;
    user:IUser;
    userId:string;
    description:string;
    files:IFile[];
    fileIds:string[];
}

export type TDeleteNews = {
    newsId:string;
    news:INew;
}

export type TGetAllNews  = {
    skip:string;
    limit:string;
}



