import { Router } from "express";
import ValidationRequest from "../../../../../middlewares/ValidationRequest";
import requestSchema from "../validations/index";
import auth from "../../../../../middlewares/Auth";

import NewsController from "../controllers/index";


const router = Router({mergeParams:true});
const ctrl = NewsController;

router
    .route("/")
    .post(ValidationRequest(requestSchema.createNews),auth,ctrl.createNews)
    .get(ValidationRequest(requestSchema.getAllNews),ctrl.getAllNews)
    .put(ValidationRequest(requestSchema.updateNews),auth,ctrl.updateNews)
    .delete(ValidationRequest(requestSchema.deleteNews),ctrl.deleteNews)

router
    .route("/new")
    .get(ValidationRequest(requestSchema.getNewsById),ctrl.getNews);

export default router;