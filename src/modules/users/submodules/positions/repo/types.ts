import { IPosition } from "../../../../../domain/entities/Position";
import {IFile} from "../../../../../domain/entities/File";
import { IUser } from "../../../../../domain/entities/User";

export type TCreatePosition= {
    name:string;
    user:IUser;
    file:IFile;
}

export type TGetPositionById = {
    positionId:string;
}

export type TGetPositions = {
    skip:string;
    limit:string;
}


export type TUpdatePosition = {
    position:IPosition
    name:string;
    user:IUser;
    file:IFile;
}

export type TDeletePosition =  {
    position:IPosition
}