import Position from "../../../../../database/models/Position";
import { TCreatePosition,TDeletePosition,TGetPositionById,TGetPositions,TUpdatePosition } from "./types";
import logger from "../../../../../shared/logger";
import { configurations } from "../../../../../config";

class PositionRepo {
    async createPosition(data:TCreatePosition){
        try{
            const newPosition = new Position(data)

            await newPosition.save();

            return newPosition;
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "company.repo",
                  method: "POST",
                  methodName: "createPosition",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `/positions`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [company.repo] createPosition"
              );
              console.log(`ERROR: [company.repo] createPosition: ${error}`);
              throw error;
        }
    }

    async getPositions(data:TGetPositions){
        try{
            return await Position.find({
                isDeleted:false
            })
            .populate("file")
            .skip(+data.skip).limit(+data.limit)
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "company.repo",
                  method: "GET",
                  methodName: "getCompanies",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/company`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [company.repo] getCompanies"
              );
            console.log(`ERROR: [company.repo] getCompanies: ${error}`);
            throw error;
        }
    }

    async getPositionById(data:TGetPositionById){
        try{
            return await Position.findOne({
                _id:data.positionId,
                isDeleted:false
            })
            .populate("file")
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "company.repo",
                  method: "GET",
                  methodName: "getPositionById",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/company`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [company.repo] getPositionById"
              );
            console.log(`ERROR: [company.repo] getPositionById: ${error}`);
            throw error;
        }
    }



    async updatePosition(data:TUpdatePosition){
        try{
            return await Position.findByIdAndUpdate({
                isDeleted:false,
                _id:data.position.id
            },{
                ...data
            },{
                new:true
            })
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "company.repo",
                  method: "PUT",
                  methodName: "updatePosition",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `/positions`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [company.repo] updatePosition"
              );
            console.log(`ERROR: [company.repo] updatePosition: ${error}`);
            throw error;
        }
    }

    async deletePosition(data:TDeletePosition){
        try{
            return await Position.findByIdAndUpdate({
                isDeleted:false,
                _id:data.position.id
            },{
              isDeleted:true
            })
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "position.repo",
                  method: "DELETE",
                  methodName: "deletePosition",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `users/company`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [position.repo] deletePosition"
              );
            console.log(`ERROR: [position.repo] deletePosition: ${error}`);
            throw error;
        }
    }
}

export default PositionRepo;