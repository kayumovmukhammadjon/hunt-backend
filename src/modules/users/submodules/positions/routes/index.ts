import { Router } from "express";
import ValidationRequest from "../../../../../middlewares/ValidationRequest";
import requestSchema from "../validations/index";
import auth from "../../../../../middlewares/Auth";

import UserController from "../controllers/index";


const router = Router({mergeParams:true});
const ctrl = UserController;

router
    .route("/")
    .post(ValidationRequest(requestSchema.createPosition),auth,ctrl.createPosition)
    .get(ValidationRequest(requestSchema.getAllPositions),ctrl.getPositions)
    .put(ValidationRequest(requestSchema.updatePosition),ctrl.updatePosition)
    .delete(ValidationRequest(requestSchema.deletePosition),ctrl.deletePosition)

router
    .route("/position")
    .get(ValidationRequest(requestSchema.getPosition),auth,ctrl.getPosition);

export default router;