import { Request,Response } from "express";
import PositionService from "../services/index";

class PositionController {
    async createPosition(req:Request,res:Response){
        try{
            const {
                code,
                message,
                data:result
            } = await PositionService.createPosition(req.body);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Position controller [createPosition] error: ${error}`);
        }
    }

    async getPositions(req:Request,res:Response){
        try{
            const query:any = req.query;

            const {
                code,
                message,
                data:result
            } = await PositionService.getPositions(query);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Position controller [getCompanies] error: ${error}`);
        }
    }

    async getPosition(req:Request,res:Response){
        try{
            const query:any= req.query;
            const {
                code,
                message,
                data:result
            } = await PositionService.getPosition(query);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Position controller [getCompany] error: ${error}`);
        }
    }

    async updatePosition(req:Request,res:Response){
        try{
            const {
                code,
                message,
                data: result,
              } = await PositionService.updatePosition(req.body);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Position controller [updateCompany] error: ${error}`);
        }
    }

    async deletePosition(req:Request,res:Response){
        try{
            const query:any = req.query
            const {
                code,
                message,
                data: result,
              } = await PositionService.deletePosition(query);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Position controller [deleteCompany] error: ${error}`);
        }
    }
}

export default new PositionController();