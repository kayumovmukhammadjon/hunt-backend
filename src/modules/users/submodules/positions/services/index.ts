import { TCreatePosiion,TDeletePosition,TGetPosition,TGetPositionById,TGetPositions,TUpdatePosition } from "./types";
import logger from "../../../../../shared/logger";
import { configurations } from "../../../../../config";
import PositionRepo from "../repo";
import UserService from "../../../services/index";
import FileService from "../../../../files/service/index";
import {ok,fail} from "../../../../../utils/response";


const repo = new PositionRepo();

class PositionService {
    async createPosition(data:TCreatePosiion){
      try{
        const user = await UserService.getUserById({
          userId:data.userId
        })

        if(!user){
          return fail(404,"User Not Found!")
        }

        data.user = user;
      
        const file = await FileService.getFileById(data.fileId);

        if(!file){
          return fail(404,"File Not Found");
        }

        data.file = file

        const newPosition = await repo.createPosition(data);

        return ok(newPosition);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "position.service",
            method: "POST",
            methodName: "createPosition",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: data,
              query: null,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [position.service] createPosition"
        );
        console.log(`ERROR: [position.service] createPosition: ${error}`);
        throw error;
      }
    }

    async updatePosition(data:TUpdatePosition){
      try{
        const position = await repo.getPositionById({
          positionId:data.positionId
        });

        if(!position){
          return fail(404,"Position Not Found!")
        }

        data.position = position;

        const user = await UserService.getUserById({
          userId:data.userId
        })

        if(!user){
          return fail(404,"User Not Found!")
        }

        data.user = user;
      
        const file = await FileService.getFileById(data.fileId);

        if(!file){
          return fail(404,"File Not Found");
        }

        data.file = file

        const newPosition = await repo.updatePosition(data);

        return ok(newPosition);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "company.service",
            method: "PUT",
            methodName: "updateCompany",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: data,
              query: null,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [company.service] updateCompany"
        );
        console.log(`ERROR: [company.service] updateCompany: ${error}`);
        throw error;
      }
    }

    async getPositions(data:TGetPositions){
      try{
        const positions = await repo.getPositions(data);

        return ok(positions);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "company.service",
            method: "GET",
            methodName: "getPositions",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [company.service] getPositions"
        );
        console.log(`ERROR: [company.service] getPositions: ${error}`);
        throw error;
      }
    }

    async getPosition(data:TGetPosition){
      try{
        const position = await repo.getPositionById({
          positionId:data.positionId
        })

        if(!position){
          return fail(404,"Position Not Found!")
        }

        data.position = position;
      

        return ok(position);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "company.service",
            method: "GET",
            methodName: "getCompany",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [company.service] getCompany"
        );
        console.log(`ERROR: [company.service] getCompany: ${error}`);
        throw error;
      }
    }

    async getPositionById(data:TGetPositionById){
      try{
        return await repo.getPositionById({
          positionId:data.positionId
        })
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "company.service",
            method: "GET",
            methodName: "getCompanyById",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [company.service] getCompanyById"
        );
        console.log(`ERROR: [company.service] getCompanyById: ${error}`);
        throw error;
      }
    }

    async deletePosition(data:TDeletePosition){
      try{
        const position = await repo.getPositionById({
          positionId:data.positionId
        })

        if(!position){
          return fail(404,"Company Not Found!")
        }

        data.position = position;

        const deletedPosition = await repo.deletePosition(data);
      
        return ok(deletedPosition);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "company.service",
            method: "GET",
            methodName: "createCompany",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: data,
              query: null,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [company.service] createCompany"
        );
        console.log(`ERROR: [company.service] createCompany: ${error}`);
        throw error;
      }
    }
}

export default new PositionService();