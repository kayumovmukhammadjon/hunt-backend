import { IPosition } from "../../../../../domain/entities/Position";
import { IFile } from "../../../../../domain/entities/File"
import {IUser} from "../../../../../domain/entities/User";

export type TCreatePosiion= {
    user:IUser;
    userId:string;
    name:string;
    file:IFile;
    fileId:string;
}

export type TGetPosition = {
    positionId:string;
    position:IPosition
}
export type TGetPositionById = {
    positionId:string;
}

export type TGetPositions = {
    skip:string;
    limit:string;
}

export type TUpdatePosition = {
    position:IPosition;
    positionId:string;
    user:IUser;
    userId:string;
    name:string;
    file:IFile;
    fileId:string;
}

export type TDeletePosition =  {
    position:IPosition;
    positionId:string;
}