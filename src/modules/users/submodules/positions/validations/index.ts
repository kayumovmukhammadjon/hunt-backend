import Joi from "joi";

interface PositionSchema {
    createPosition:Joi.Schema<any>;
    updatePosition:Joi.Schema<any>;
    deletePosition:Joi.Schema<any>
    getAllPositions:Joi.Schema<any>
    getPosition:Joi.Schema<any>
}

const schema:PositionSchema = {
    createPosition:Joi.object().keys({
        body:Joi.object().keys({
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            fileId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            name:Joi.string().required(),
        }).required()
    }),
    updatePosition:Joi.object().keys({
        body:Joi.object().keys({
            positionId:Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            fileId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            name:Joi.string().required(),
        }).required()
    }),
    deletePosition:Joi.object().keys({
        query:Joi.object().keys({
            positionId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
        }).required()
    }),
    getAllPositions:Joi.object().keys({
        query:Joi.object().keys({
            limit: Joi.string().required(),
            skip:Joi.string().required(),
        }).required()
    }),
    getPosition:Joi.object().keys({
        query:Joi.object().keys({
            positionId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
        }).required()
    }),
}

export default schema;