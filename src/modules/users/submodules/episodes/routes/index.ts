import { Router } from "express";
import ValidationRequest from "../../../../../middlewares/ValidationRequest";
import requestSchema from "../validations/index";
import auth from "../../../../../middlewares/Auth";

import EpisodeController from "../controllers/index";


const router = Router({mergeParams:true});
const ctrl = EpisodeController;

router
    .route("/")
    .post(ValidationRequest(requestSchema.createEpisode),auth,ctrl.createEpisode)
    .get(ValidationRequest(requestSchema.getAllEpisodes),ctrl.getAllEpisodes)
    .put(ValidationRequest(requestSchema.updateEpisode),auth,ctrl.updateEpisode)
    .delete(ValidationRequest(requestSchema.deleteEpisode),auth,ctrl.deleteEpisode)

router
    .route("/episode")
    .get(ValidationRequest(requestSchema.getEpisode),ctrl.getEpisode);

export default router;