import Episode from "../../../../../database/models/Episode";
import {TCreateEpisode,TDeleteEpisode,TGetAllEpisodes,TGetEpisodeById,TUpdateEpisode} from "./types";

class EpisodeRepo {
    async create(data:TCreateEpisode){
        const newEpisode = new Episode(data)

        await newEpisode.save()

        return newEpisode;
    }

    async getEpisodeById(data:TGetEpisodeById){
        return await Episode.findOne({
            _id:data.episodeId,
            isDeleted:false
        })
    }

    async getAllEpisodes(data:TGetAllEpisodes){
        return await Episode.find({isDeleted:false}).skip(+data.skip).limit(+data.limit);
    }

    async updateEpisode(data:TUpdateEpisode){
        return await Episode.findOneAndUpdate({
            _id:data.user.id,
            isDeleted:false
        },data,{new:true})
    } 

   

    async deleteEpisode(data:TDeleteEpisode){
        return await Episode.findOneAndUpdate({
            _id:data.episode.id,
            isDeleted:false
        },{
            isDeleted:true
        })
    }
}

export default EpisodeRepo;