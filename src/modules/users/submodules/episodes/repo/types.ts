import { IUser } from "../../../../../domain/entities/User";
import {IFile} from "../../../../../domain/entities/File";
import { IEpisode } from "../../../../../domain/entities/Episode";

export type TCreateEpisode = {
    title:string;
    user:IUser;
    description:string;
    files:IFile[];
}


export type TGetEpisodeById = {
    episodeId:string;
}

export type TUpdateEpisode = {
    episode:IEpisode;
    title:string;
    user:IUser;
    description:string;
    files:IFile[];
}

export type TDeleteEpisode = {
    episode:IEpisode;
}

export type TGetAllEpisodes = {
    skip:string;
    limit:string;
}



