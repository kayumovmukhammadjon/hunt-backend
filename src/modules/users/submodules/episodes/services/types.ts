import { IUser } from "../../../../../domain/entities/User";
import {IFile} from "../../../../../domain/entities/File";
import { IEpisode } from "../../../../../domain/entities/Episode";

export type TCreateEpisode = {
    title:string;
    user:IUser;
    userId:string;
    description:string;
    files:IFile[];
    fileIds:string[];
}


export type TGetEpisodeById = {
    episodeId:string;
}

export type TUpdateEpisode = {
    episodeId:string;
    episode:IEpisode;
    title:string;
    user:IUser;
    userId:string;
    description:string;
    files:IFile[];
    fileIds:string[];
}

export type TDeleteEpisode = {
    episode:IEpisode;
    episodeId:string;
}

export type TGetAllEpisodes = {
    skip:string;
    limit:string;
}



