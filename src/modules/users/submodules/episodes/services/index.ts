import { TCreateEpisode,TDeleteEpisode,TGetAllEpisodes,TGetEpisodeById,TUpdateEpisode } from "./types";
import { configurations } from "../../../../../config";
import logger from "../../../../../shared/logger";
import EpisodeRepo from "../repo";
import { fail,ok } from "../../../../../utils/response";
import FileService from "../../../../files/service/index";

const repo = new EpisodeRepo();

class EpisodeService {
    async createEpisode(data: TCreateEpisode) {
        try {
            const newUser = await repo.create(data);

            return ok(newUser);
        } catch (error:any) {
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "episodes.service",
                  method: "POST",
                  methodName: "createEpisode",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `/users/episodes`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [episodes.service] createEpisode"
              );
              console.log(`ERROR: [episodes.service] createEpisode: ${error}`);
              return fail(500,error);
        }
    }

    async getEpisodeById(data:TGetEpisodeById){
        try{
            return await repo.getEpisodeById({
                episodeId:data.episodeId
            });
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "episodes.service",
                  method: "GET",
                  methodName: "getUserById",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `/users/episodes/episode`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [episodes.service] getUserById"
              );
            console.log(`ERROR: [episodes.service] getUserById: ${error}`);
        }
    }

    async getAllEpisodes(data:TGetAllEpisodes){
        try{
            const allUsers =  await repo.getAllEpisodes(data);

            return ok(allUsers);
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "episodes.service",
                  method: "GET",
                  methodName: "getAllEpisodes",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `getAllUsers`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [episodes.service] getAllEpisodes"
              );
            console.log(`ERROR: [episodes.service] getAllEpisodes: ${error}`);
            return fail(500,error);
        }
    }

    async getEpisode(data:TGetEpisodeById){
        try{
            const user = await repo.getEpisodeById({
                episodeId:data.episodeId
            });

            if(!user){
                return fail(404,"User Not Found!");
            }

            return ok(user);
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "episodes.service",
                  method: "GET",
                  methodName: "getEpisode",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `getEpisode`,
                  apiData: {
                    body: null,
                    query: data,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [episodes.service] getEpisode"
              );
            console.log(`ERROR: [episodes.service] getEpisode: ${error}`);
            return fail(500, error);0
        }
    }

    async updateEpisode(data:TUpdateEpisode){
        try{
            const episode = await repo.getEpisodeById({
                episodeId:data.episodeId
            })

            if(!episode){
                return fail(404,"Episode Not Found!");
            }
            
            data.episode = episode;

            if(data.fileIds.length>0){
              let files = await FileService.getFilesByIds(data.fileIds);

              data.files = files
            }

            const updatedUser =  await repo.updateEpisode(data);

            return ok(updatedUser);
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "episodes.service",
                  method: "PUT",
                  methodName: "updateEpisode",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `updateEpisode`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [episodes.service] updateEpisode"
              );
            console.log(`ERROR: [episodes.service] updateEpisode: ${error}`);
            return fail(500,error);
        }
    }

    async deleteEpisode(data:TDeleteEpisode){
        try{
            const episode = await repo.getEpisodeById({
                episodeId:data.episodeId
            });

            if(!episode){
                return fail(404,"Episode Not Found");
            }

            data.episode = episode;

            const deletedEpisode =  await repo.deleteEpisode(data);

            return ok(deletedEpisode);
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  serviceName: "episodes.service",
                  method: "DELETE",
                  methodName: "deletedEpisode",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath: `deletedEpisode`,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [episodes.service] deletedEpisode"
              );
            console.log(`ERROR: [episodes.service] deletedEpisode: ${error}`);
            return fail(500,error)
        }
    }
}


export default new EpisodeService();