import { Request,Response } from "express";
import EpisodeService from "../services/index";

class EpisodeController {
    async createEpisode(req:Request,res:Response){
        try{
            const {
                code,
                message,
                data:result
            } = await EpisodeService.createEpisode(req.body);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Episode controller [getAllEpisodes] error: ${error}`);
        }
    }

    async getAllEpisodes(req:Request,res:Response){
        try{
            const query:any = req.query;

            const {
                code,
                message,
                data:result
            } = await EpisodeService.getAllEpisodes(query);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Episode controller [getAllEpisodes] error: ${error}`);
        }
    }


    async getEpisode(req:Request,res:Response){
        try{
            const query:any = req.query;

            const {
                code,
                message,
                data: result,
              } = await EpisodeService.getEpisode(query);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Episode controller [getEpisode] error: ${error}`);
        }
    }

    async updateEpisode(req:Request,res:Response){
        try{
            const {
                code,
                message,
                data: result,
              } = await EpisodeService.updateEpisode(req.body);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Episode controller [updateEpisode] error: ${error}`);
        }
    }

    async deleteEpisode(req:Request,res:Response){
        try{
            const query:any = req.query;

            const {
                code,
                message,
                data: result,
              } = await EpisodeService.deleteEpisode(query);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Episode controller [deleteEpisode] error: ${error}`);
        }
    }
}

export default new EpisodeController();