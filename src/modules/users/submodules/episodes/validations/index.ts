import Joi from "joi";

interface UserSchema {
    getEpisode:Joi.Schema<any>;
    updateEpisode:Joi.Schema<any>;
    deleteEpisode:Joi.Schema<any>;
    getAllEpisodes:Joi.Schema<any>;
    createEpisode:Joi.Schema<any>;
}

const schema:UserSchema = {
    createEpisode:Joi.object().keys({
        body:Joi.object().keys({
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            description:Joi.string().required(),
            title:Joi.string().required(),
            fileIds: Joi.array().items(Joi.string().pattern(/^[0-9a-fA-F]{24}$/)).required(),
        })
    }),
    getEpisode:Joi.object().keys({
        query:Joi.object().keys({
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
        }).required()
    }),
    getAllEpisodes:Joi.object().keys({
        query:Joi.object().keys({
            skip:Joi.string().required(),
            limit:Joi.string().required()
        }).required()
    }),
    updateEpisode:Joi.object().keys({
        body:Joi.object().keys({
            episodeId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            description:Joi.string().required(),
            title:Joi.string().required(),
            fileIds: Joi.array().items(Joi.string().pattern(/^[0-9a-fA-F]{24}$/)).required(),
        })
    }),
    deleteEpisode:Joi.object().keys({
        query:Joi.object().keys({
            episodeId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
        }).required()
    })
}

export default schema;