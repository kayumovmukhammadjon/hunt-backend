import { Request,Response } from "express";
import UserService from "../services/index";

class UserController {
    async getAllUsers(req:Request,res:Response){
        try{
            const query:any = req.query;
            const {
                code,
                message,
                data:result
            } = await UserService.getAllUsers(query);

            res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`User controller [getAllUsers] error: ${error}`);
        }
    }


    async getUser(req:Request,res:Response){
        try{
            const query:any = req.query;

            const {
                code,
                message,
                data: result,
              } = await UserService.getUser(query);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`User controller [getUser] error: ${error}`);
        }
    }

    async updateUser(req:Request,res:Response){
        try{
            const {
                code,
                message,
                data: result,
              } = await UserService.updateUser(req.body);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`User controller [updateUser] error: ${error}`);
        }
    }

    async deleteUser(req:Request,res:Response){
        try{
            const {
                code,
                message,
                data: result,
              } = await UserService.deleteUser(req.body);
        
              res.status(code).send({
                code,
                msg: message,
                data: result,
            });
        }catch(error:any){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`User controller [deleteUser] error: ${error}`);
        }
    }
}

export default new UserController();