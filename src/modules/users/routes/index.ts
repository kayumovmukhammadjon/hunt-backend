import { Router } from "express";
import ValidationRequest from "../../../middlewares/ValidationRequest";
import requestSchema from "../validations/index";
import auth from "../../../middlewares/Auth";

import UserController from "../controllers/index";
import CompanyRoutes from "../submodules/companies/routes/index";
import PositionRoutes from "../submodules/positions/routes/index";
import ResumeRoutes from "../submodules/resumes/routes/index";
import VacancyRoutes from "../submodules/vacancies/routes/index";


const router = Router({mergeParams:true});

const ctrl = UserController;

router.use("/companies",CompanyRoutes);
router.use("/positions",PositionRoutes);
router.use("/resumes",ResumeRoutes);
router.use("/vacancies",VacancyRoutes);


router
    .route("/")
    .get(ValidationRequest(requestSchema.getAllUsers),ctrl.getAllUsers)
    .put(ValidationRequest(requestSchema.updateUser),ctrl.updateUser)
    .delete(ValidationRequest(requestSchema.deleteUser),ctrl.deleteUser)

router
    .route("/user")
    .get(ValidationRequest(requestSchema.getUser),ctrl.getUser);

export default router;