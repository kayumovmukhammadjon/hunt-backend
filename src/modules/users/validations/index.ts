import Joi from "joi";

interface UserSchema {
    getUser:Joi.Schema<any>;
    updateUser:Joi.Schema<any>;
    deleteUser:Joi.Schema<any>
    getAllUsers:Joi.Schema<any>
}

const schema:UserSchema = {
    getUser:Joi.object().keys({
        query:Joi.object().keys({
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
        }).required()
    }),
    getAllUsers:Joi.object().keys({
        query:Joi.object().keys({
            skip:Joi.string().required(),
            limit:Joi.string().required()
        }).required()
    }),
    updateUser:Joi.object().keys({
        body:Joi.object().keys({
            userId: Joi.string()
            .pattern(/^[0-9a-fA-F]{24}$/)
            .required(),
            phoneNumber: Joi.string()
            .pattern(/^998\d{9}$/)
            .required(),
            username:Joi.string().optional(),
            fullName:Joi.string().optional()
        })
    }),
    deleteUser:Joi.object().keys({
        query:Joi.object().keys({
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
        }).required()
    })
}

export default schema;