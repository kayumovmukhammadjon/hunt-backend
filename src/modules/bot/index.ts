import { Bot, Context, InputFile } from "grammy";
import { configurations } from "../../config";
import ctrl from "./controllers";
import path from "path";

export function startBot() {
  const bot = new Bot<Context>(configurations.bot.token);

  bot.command("start", async (ctx) => {
    const cretateUser = await ctrl.createNewUser(
      ctx.from?.id.toString() || "",
      ctx.from?.username || "",
      ctx.from?.first_name || ""
    );

    const coverPath = path.resolve(__dirname, "../../../assets/cover.png");
    ctx.replyWithPhoto(new InputFile(coverPath), {
      caption:
        `Hey ${ctx.from?.first_name}!` +
        `\n\n<b>Let's start!</b>` +
        `\nPlease click the button to open the application Huntme Group`,
      parse_mode: "HTML",
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: "Open Huntme",
              web_app: {
                url: "https://app.huntmegroup.com/dashboard",
              },
            },
          ],
        ],
      },
    });

    // ctx.reply(
    //   `Hey ${ctx.from?.first_name}!` +
    //     `\n\n<b>Let's start!</b>` +
    //     `\nPlease click the button to open the application Huntme Group`,
    //   {
    //     parse_mode: "HTML",
    //     reply_markup: {
    //       inline_keyboard: [
    //         [
    //           {
    //             text: "Open Huntme",
    //             web_app: {
    //               url: "https://khm.app",
    //             },
    //           },
    //         ],
    //       ],
    //     },
    //   }
    // );

  });
  bot.start();
}
