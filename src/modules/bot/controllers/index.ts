import User from "../../../database/models/User";
import { IUser } from "../../../domain/entities/User";

const createNewUser = async (
  id: string,
  username: string,
  fullName: string
): Promise<IUser | null> => {
  const findUser = User.findOne({ id });
  if (findUser) {
    return findUser;
  } else {
    const user = new User<IUser>({
      id,
      username,
      fullName: fullName,
      phoneNumber: "",
      verified: false,
      verificationCode: 0,
      verificationCodeExpireTime: new Date(),
      chatId:"",
      isDeleted:false
    });

    await user.save();
    return user;
  }
};

export default {
  createNewUser,
};
