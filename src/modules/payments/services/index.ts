import TransactionService from "../submodules/transaction/service/index";
import PaymentDetailService from "../submodules/payment-details/services/index"
import PaymeMethod from "../../../integration/payme/constants/payment-methods";
import PaymeErrors from "../../../integration/payme/constants/payme-errors";
import TransactionState from "../../../integration/payme/constants/transaction-states";
import { configurations } from "../../../config/index";

class PaymentsService {
  async payme(data: any) {
    const { method, params, id } = data;
    switch (method) {
      case PaymeMethod.CheckPerformTransaction: {
        return this.checkPerformTransaction(params, id);
      }

      case PaymeMethod.CreateTransaction: {
        return this.createTransaction(params, id);
      }

      case PaymeMethod.PerformTransaction: {
        return this.performTransaction(params, id);
      }

      case PaymeMethod.CancelTransaction: {
        return this.cancelTransaction(params, id);
      }

      case PaymeMethod.CheckTransaction: {
        return this.checkTransaction(params, id);
      }
    }
  }

  private async checkPerformTransaction(params: any, id: any) {
    let { amount } = params;

    amount = Math.floor(amount / 100);

    if (!params.account.payment_id) {
      return {
        id: id,
        error: {
          code: PaymeErrors.PaymentDetailNotFound.code,
          message: PaymeErrors.PaymentDetailNotFound.message,
        },
      };
    }

    const paymentDetail = await PaymentDetailService.getPaymentDetailByPaymentId({
      paymentId:params.account.payment_id
    })

    if (!paymentDetail) {
      return {
        id: id,
        error: {
          code: PaymeErrors.PaymentDetailNotFound.code,
          message: PaymeErrors.PaymentDetailNotFound.message,
        },
      };
    }
    if (
      amount !=
      paymentDetail.price
    ) {
      return {
        id: id,
        error: {
          code: PaymeErrors.InvalidAmount.code,
          message: PaymeErrors.InvalidAmount.message,
        },
      };
    }

    return {
      result: { allow: true },
      detail: {
        receipt_type: 0,
      },
    };
  }

  private async createTransaction(params: any, ids: any) {
    const { id } = params;
    const {
      account: { payment_id },
      time,
    } = params;
    let { amount } = params;
    amount = Math.floor(amount / 100);

    const resultOfPerformTransaction = await this.checkPerformTransaction(
      params,
      id
    );

    if (!resultOfPerformTransaction.result) {
      return resultOfPerformTransaction;
    }

    let transaction = await TransactionService.getByTransactionId({
      transactionId: id,
    });

    if (transaction) {
      if (parseInt(transaction.state, 10) !== TransactionState.Pending) {
        return {
          id: id,
          error: {
            code: PaymeErrors.CantDoOperation.code,
            message: PaymeErrors.CantDoOperation.message,
          },
        };
      }

      const currentTime = Date.now();

      const expirationTime =
        (currentTime - parseInt(transaction.createTime, 10)) / 60000 < 12; // 12m

      if (!expirationTime) {
        await TransactionService.updateByTransactionId(
          {
            transactionId: params.id,
          },
          {
            state: TransactionState.PendingCanceled,
            reason: 4,
          }
        );

        return {
          id: id,
          error: {
            code: PaymeErrors.CantDoOperation.code,
            message: PaymeErrors.CantDoOperation.message,
          },
        };
      }

      return {
        result: {
          create_time: parseInt(transaction.createTime),
          transaction: transaction.transactionId,
          state: TransactionState.Pending,
        },
      };
    }

    transaction = await TransactionService.getByPaymentIndex({
      index: payment_id,
    });

    if (transaction) {
      if (parseInt(transaction.state, 10) === TransactionState.Paid)
        return {
          id: id,
          error: {
            code: PaymeErrors.AlreadyDone.code,
            message: PaymeErrors.AlreadyDone.message,
          },
        };

      if (parseInt(transaction.state, 10) === TransactionState.Pending) {
        return {
          id: id,
          error: {
            code: PaymeErrors.Pending.code,
            message: PaymeErrors.Pending.message,
          },
        };
      }
    }

    const paymentDetail = await PaymentDetailService.getPaymentDetailByPaymentId({
      paymentId:params.account.payment_id
    })

    if (!paymentDetail) {
      return {
        id: id,
        error: {
          code: PaymeErrors.PaymentDetailNotFound.code,
          message: PaymeErrors.PaymentDetailNotFound.message,
        },
      };
    }

    const newTransaction = await TransactionService.create({
      state: TransactionState.Pending.toString(),
      amount,
      paymentDetail: paymentDetail,
      createTime: time,
      transactionId: id,
      index: paymentDetail.id,
    });

    return {
      result: {
        create_time: parseInt(newTransaction.createTime),
        transaction: newTransaction.transactionId,
        state: TransactionState.Pending,
        receivers: [
          {
            id: configurations.payme.merchantId,
            amount: paymentDetail.price * 100,
          },
        ],
      },
    };
  }

  private async performTransaction(params: any, id: any) {
    const currentTime = Date.now();

    const transaction = await TransactionService.getByTransactionId({
      transactionId: params.id,
    });

    if (!transaction) {
      return {
        id: id,
        error: {
          code: PaymeErrors.TransactionNotFound.code,
          message: PaymeErrors.TransactionNotFound.message,
        },
      };
    }

    if (parseInt(transaction.state, 10) !== TransactionState.Pending) {
      if (parseInt(transaction.state, 10) !== TransactionState.Paid) {
        return {
          id: id,
          error: {
            code: PaymeErrors.CantDoOperation.code,
            message: PaymeErrors.CantDoOperation.message,
          },
        };
      }

      return {
        result: {
          perform_time: parseInt(transaction.performTime, 10),
          transaction: transaction.transactionId,
          state: TransactionState.Paid,
        },
      };
    }

    const expirationTime =
      (currentTime - parseInt(transaction.createTime, 10)) / 60000 < 12; // 12m

    if (!expirationTime) {
      await TransactionService.updateByTransactionId(
        {
          transactionId: params.id,
        },
        {
          state: TransactionState.PendingCanceled,
          reason: 4,
          cancelTime: currentTime.toString(),
        }
      );

      return {
        id: id,
        error: {
          code: PaymeErrors.CantDoOperation.code,
          message: PaymeErrors.CantDoOperation.message,
        },
      };
    }

    await TransactionService.updateByTransactionId(
      {
        transactionId: params.id,
      },
      {
        state: TransactionState.Paid,
        performTime: currentTime.toString(),
      }
    );

    return {
      result: {
        transaction: transaction.transactionId,
        perform_time: currentTime,
        state: TransactionState.Paid,
      },
    };
  }

  private async cancelTransaction(params: any, id: any) {
    const transaction = await TransactionService.getByTransactionId({
      transactionId: params.id,
    });

    if (!transaction) {
      return {
        id: id,
        error: {
          code: PaymeErrors.TransactionNotFound.code,
          message: PaymeErrors.TransactionNotFound.message,
        },
      };
    }

    const currentTime = Date.now();

    if (parseInt(transaction.state, 10) > 0) {
      await TransactionService.updateByTransactionId(
        {
          transactionId: params.id,
        },
        {
          state: -Math.abs(parseInt(transaction.state, 10)),
          reason: params.reason,
          cancelTime: currentTime.toString(),
        }
      );
    }

    return {
      result: {
        cancel_time: parseInt(transaction.cancelTime, 10) || currentTime,
        transaction: transaction.transactionId,
        state: -Math.abs(parseInt(transaction.state)),
      },
    };
  }

  private async checkTransaction(params: any, id: any) {
    const transaction = await TransactionService.getByTransactionId({
      transactionId: params.id,
    });

    if (!transaction) {
      return {
        id: id,
        error: {
          code: PaymeErrors.TransactionNotFound.code,
          message: PaymeErrors.TransactionNotFound.message,
        },
      };
    }

    return {
      result: {
        create_time: parseInt(transaction.createTime, 10),
        perform_time: transaction.performTime
          ? parseInt(transaction.performTime, 10)
          : 0,
        cancel_time: transaction.cancelTime
          ? parseInt(transaction.cancelTime, 10)
          : 0,
        transaction: transaction.transactionId,
        state: parseInt(transaction.state, 10),
        reason: transaction.reason,
      },
    };
  }
}

export default new PaymentsService();
