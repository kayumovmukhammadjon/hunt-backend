import { Request, Response } from 'express';
import PaymentService from '../services/index';

class PaymentContoller {
    async payme(req: Request, res: Response) {
        try {
            const data = await PaymentService.payme(req.body);

            res.status(200).send(data);

        } catch (err) {
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`PaymentController controller [payme] error: ${err}`);
        }
    }
}

export default new PaymentContoller();