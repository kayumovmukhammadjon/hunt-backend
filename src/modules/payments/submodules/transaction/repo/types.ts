import { IPaymentDetail } from "../../../../../domain/entities/PaymentDetail";
import { IVacancy } from "../../../../../domain/entities/Vacancy";

export type TCreateTransaction = {
    paymentDetail:IPaymentDetail
    amount:number,
    createTime:string,
    transactionId:string,
    state?:string,
    index:number,
}

export type TGetTransactionByTransactionId = {
    transactionId:string,
}

export type TUpdateTransactionParams =TGetTransactionByTransactionId;

export type TUpdateSTransactionData = {
    state?:number,
    performTime?:string,
    reason?:number,
    cancelTime?:string,
}

export type TGetTransactionByPaymentIndex = {
    index:number
}

