import {
  TCreateTransaction,
  TGetTransactionByPaymentIndex,
  TGetTransactionByTransactionId,
  TUpdateSTransactionData,
  TUpdateTransactionParams,
} from "./types";
import Transaction from "../../../../../database/models/Transaction";
import { configurations } from "../../../../../config/index"
import logger from "../../../../../shared/logger"

class TransactionRepo {
  async create(data: TCreateTransaction) {
    try {
      const {
        paymentDetail,
        amount,
        createTime,
        transactionId,
        state,
        index,
      } = data;

      const transaction = await Transaction.create({
        paymentDetail,
        amount,
        createTime,
        transactionId,
        state,
        index ,
      });

      return transaction;
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          repoName: "transaction.repo",
          method: "POST",
          methodName: "create",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [transaction.repo] create"
      );
      console.log(`ERROR: [transaction.repo] create: ${error}`);
      throw error;
    }
  }

  async getByTransactionId(params: TGetTransactionByTransactionId) {
    try {
      const transaction = await Transaction.findOne({
        transactionId: params.transactionId,
      });

      return transaction;
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          repoName: "transaction.repo",
          method: "GET",
          methodName: "getByTransactionId",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: null,
            query: null,
            params: params,
          },
          message: error.message,
        }),
        "ERROR: [transaction.repo] getByTransactionId"
      );
      console.log(`ERROR: [transaction.repo] getByTransactionId: ${error}`);
      throw error;
    }
  }

  async updateByTransactionId(
    params: TUpdateTransactionParams,
    data: TUpdateSTransactionData
  ) {
    try {
      const { transactionId } = params;
      const { state, performTime, reason, cancelTime } = data;

      const transaction = await Transaction.updateOne(
        {
          transactionId: transactionId,
        },
        {
          state,
          performTime,
          reason,
          cancelTime,
        }
      );

      return transaction;
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          repoName: "transaction.repo",
          method: "POST",
          methodName: "updateByTransactionId",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: data,
            query: null,
            params: params,
          },
          message: error.message,
        }),
        "ERROR: [transaction.repo] updateByTransactionId"
      );
      console.log(`ERROR: [transaction.repo] updateByTransactionId: ${error}`);
      throw error;
    }
  }

  async getByPaymentIndex(params: TGetTransactionByPaymentIndex) {
    try {
      const transaction = await Transaction.findOne({
        index: params.index,
      });

      return transaction;
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          repoName: "transaction.repo",
          method: "GET",
          methodName: "getByPaymentIndex",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: null,
            query: null,
            params: params,
          },
          message: error.message,
        }),
        "ERROR: [transaction.repo] getByPaymentIndex"
      );
      console.log(
        `ERROR: [transaction.repo] getByPaymentIndex: ${error}`
      );
      throw error;
    }
  }
}

export default TransactionRepo;
