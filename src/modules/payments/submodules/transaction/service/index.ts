import { configurations } from "../../../../../config/index";
import logger from "../../../../../shared/logger"
import { ok, fail } from "../../../../../utils/response";
import TransactionRepo from "../repo/index";
import {
  TCreateTransaction,
  TGetTransactionByTransactionId,
  TUpdateTransactionParams,
  TUpdateTransactionData,
  TGetTransactionByPaymentIndex,
} from "./types";

const repo = new TransactionRepo();

class TransactionService {
  async create(data: TCreateTransaction) {
    try {
      const {
        paymentDetail,
        amount,
        createTime,
        transactionId,
        state,
        index,
      } = data;

      const transaction = await repo.create({
        paymentDetail,
        amount,
        createTime,
        transactionId,
        state,
        index,
      });

      return transaction;
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "transaction.service",
          method: "POST",
          methodName: "create",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [transaction.service] create"
      );
      console.log(`ERROR: [transaction.service] create: ${error}`);
      throw error;
    }
  }

  async getByTransactionId(params: TGetTransactionByTransactionId) {
    try {
      const subscriptionTransaction = await repo.getByTransactionId(params);

      return subscriptionTransaction;
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "transaction.service",
          method: "GET",
          methodName: "getByTransactionId",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: null,
            query: null,
            params: params,
          },
          message: error.message,
        }),
        "ERROR: [transaction.service] getByTransactionId"
      );
      console.log(`ERROR: [transaction.service] getByTransactionId: ${error}`);
      throw error;
    }
  }

  async updateByTransactionId(
    params: TUpdateTransactionParams,
    data: TUpdateTransactionData
  ) {
    try {
      const { transactionId } = params;
      const { state, performTime, reason, cancelTime } = data;

      const subscriptionTransaction = await repo.updateByTransactionId(
        {
          transactionId,
        },
        {
          state,
          performTime,
          reason,
          cancelTime,
        }
      );

      return subscriptionTransaction;
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "transaction.service",
          method: "POST",
          methodName: "updateByTransactionId",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: data,
            query: null,
            params: params,
          },
          message: error.message,
        }),
        "ERROR: [transaction.service] updateByTransactionId"
      );
      console.log(
        `ERROR: [transaction.service] updateByTransactionId: ${error}`
      );
      throw error;
    }
  }

  async getByPaymentIndex(params: TGetTransactionByPaymentIndex) {
    try {
      const subscriptionTransaction = await repo.getByPaymentIndex(params);

      return subscriptionTransaction;
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "transaction.service",
          method: "GET",
          methodName: "getByOptoPaymentIndex",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: null,
            query: null,
            params: params,
          },
          message: error.message,
        }),
        "ERROR: [transaction.service] getByOptoPaymentIndex"
      );
      console.log(
        `ERROR: [transaction.service] getByOptoPaymentIndex: ${error}`
      );
      throw error;
    }
  }
}

export default new TransactionService();
