import { ICompany } from "../../../../../domain/entities/Company";
import { ICurrency } from "../../../../../domain/entities/Currency";
import { IFile } from "../../../../../domain/entities/File"
import { IResume } from "../../../../../domain/entities/Resume";
import { IUser } from "../../../../../domain/entities/User";
import { IVacancy } from "../../../../../domain/entities/Vacancy";

export type TCreatePaymentDetails = {
    curreny?:ICurrency;
    price:number;
    vacancy?:IVacancy;
    resume?:IResume;
}


export type TGetPaymentDetailByPaymentId ={
    paymentId:string;
}