import PaymentDetail from "../../../../../database/models/PaymentDetail";
import { TCreatePaymentDetails,TGetPaymentDetailByPaymentId } from "./types";
import logger from "../../../../../shared/logger";
import { configurations } from "../../../../../config";

class PaymentDetailRepo {
    async create(data:TCreatePaymentDetails){
        try{
            const newPaymentDetail = new PaymentDetail(data)

            await newPaymentDetail.save();

            return newPaymentDetail;
        }catch(error:any){
            logger.error(
                JSON.stringify({
                  logLevel: "ERROR",
                  projectName: `${configurations.application.name}`,
                  repoName: "payment-details.repo",
                  method: "POST",
                  methodName: "create",
                  brandId: null,
                  userId: null,
                  customerId: null,
                  exception: error.name,
                  apiPath:null,
                  apiData: {
                    body: data,
                    query: null,
                    params: null,
                  },
                  message: error.message,
                }),
                "ERROR: [company.repo] create"
              );
              console.log(`ERROR: [company.repo] create: ${error}`);
              throw error;
        }
    }

    async getPaymentDetailsByPaymentId(data:TGetPaymentDetailByPaymentId){
      try{
        return await PaymentDetail.findOne({
          _id:data.paymentId,
          isDeleted:false
        })
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            repoName: "payment-details.repo",
            method: "POST",
            methodName: "getPaymentDetailsByPaymentId",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath:null,
            apiData: {
              body: null,
              query: data,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [company.repo] getPaymentDetailsByPaymentId"
        );
        console.log(`ERROR: [company.repo] getPaymentDetailsByPaymentId: ${error}`);
        throw error;
      }
    }
   
}

export default PaymentDetailRepo;