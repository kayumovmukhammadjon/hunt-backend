import logger from "../../../../../shared/logger";
import { configurations } from "../../../../../config";
import PaymentDetailRepo from "../repo";
import UserService from "../../../services/index";
import FileService from "../../../../files/service/index";
import { TCreatePaymentDetails,TGetPaymentDetailByPaymentId } from "./types";
import CurrencyService from "../../../../currency/services/index";
import ResumeService from "../../../../users/submodules/resumes/services/index";
import VacancyService from "../../../../users/submodules/vacancies/services/index";

const repo = new PaymentDetailRepo();

class PaymentDetailService {
    async create(data:TCreatePaymentDetails){
      try{
        const currencies = (await CurrencyService.getAllCurrencies()).data;


        if(data.resume){
          data.currency = currencies[0];
          data.price = 100
        }else{
          data.currency = currencies[0];
          data.price = 100
        }

        return await repo.create(data);
      }catch(error:any){
        logger.error(
          JSON.stringify({
            logLevel: "ERROR",
            projectName: `${configurations.application.name}`,
            serviceName: "payment-details.service",
            method: "POST",
            methodName: "create",
            brandId: null,
            userId: null,
            customerId: null,
            exception: error.name,
            apiPath: null,
            apiData: {
              body: data,
              query: null,
              params: null,
            },
            message: error.message,
          }),
          "ERROR: [payment-details.service] create"
        );
        console.log(`ERROR: [payment-details.service] create: ${error}`);
        throw error;
      }
    }

  async getPaymentDetailByPaymentId(data:TGetPaymentDetailByPaymentId){
    try{
      return await repo.getPaymentDetailsByPaymentId(data);
    }catch(error:any){
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "payment-details.service",
          method: "GET",
          methodName: "getPaymentDetailByPaymentId",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [payment-details.service] getPaymentDetailByPaymentId"
      );
      console.log(`ERROR: [payment-details.service] getPaymentDetailByPaymentId: ${error}`);
      throw error;
    }
  }
}

export default new PaymentDetailService();