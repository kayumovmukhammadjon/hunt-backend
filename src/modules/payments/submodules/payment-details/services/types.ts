import { ICurrency } from "../../../../../domain/entities/Currency";
import { IResume } from "../../../../../domain/entities/Resume";
import { IVacancy } from "../../../../../domain/entities/Vacancy";

export type TCreatePaymentDetails = {
    vacancy?:IVacancy;
    resume?:IResume;
    currency?:ICurrency|null,
    price:number;
}

export type TGetPaymentDetailByPaymentId = {
    paymentId:string;
}