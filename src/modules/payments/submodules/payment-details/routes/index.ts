import { Router } from "express";
import ValidationRequest from "../../../../../middlewares/ValidationRequest";
import requestSchema from "../validations/index";
import auth from "../../../../../middlewares/Auth";

import UserController from "../controllers/index";


const router = Router({mergeParams:true});
const ctrl = UserController;

router
    .route("/")
    .post(ValidationRequest(requestSchema.createCompany),ctrl.createCompany)
    .get(ValidationRequest(requestSchema.getAllCompanies),ctrl.getCompanies)
    .put(ValidationRequest(requestSchema.updateCompany),ctrl.updateCompany)
    .delete(ValidationRequest(requestSchema.deleteCompany),ctrl.deleteCompany)

router
    .route("/user")
    .get(ValidationRequest(requestSchema.getCompaniesByUserId),auth,ctrl.getCompaniesByUserId);

router
    .route("/company")
    .get(ValidationRequest(requestSchema.getCompany),auth,ctrl.getCompany);

export default router;