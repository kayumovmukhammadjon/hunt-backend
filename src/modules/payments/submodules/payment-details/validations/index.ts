import Joi from "joi";

interface CompanySchema {
    createCompany:Joi.Schema<any>;
    updateCompany:Joi.Schema<any>;
    deleteCompany:Joi.Schema<any>
    getAllCompanies:Joi.Schema<any>
    getCompaniesByUserId:Joi.Schema<any>
    getCompany:Joi.Schema<any>
}

const schema:CompanySchema = {
    createCompany:Joi.object().keys({
        body:Joi.object().keys({
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            fileId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            name:Joi.string().required(),
            phoneNumber:Joi.string().required(),
        }).required()
    }),
    updateCompany:Joi.object().keys({
        body:Joi.object().keys({
            companyId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            fileId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            name:Joi.string().required(),
            phoneNumber:Joi.string().required(),
        }).required()
    }),
    deleteCompany:Joi.object().keys({
        query:Joi.object().keys({
            companyId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
        }).required()
    }),
    getAllCompanies:Joi.object().keys({
        query:Joi.object().keys({
            limit: Joi.string().required(),
            skip:Joi.string().required(),
        }).required()
    }),
    getCompaniesByUserId:Joi.object().keys({
        query:Joi.object().keys({
            userId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
            limit: Joi.string().required(),
            skip:Joi.string().required(),
        }).required()
    }),
    getCompany:Joi.object().keys({
        query:Joi.object().keys({
            companyId: Joi.string()
                .pattern(/^[0-9a-fA-F]{24}$/)
                .required(),
        }).required()
    }),
}

export default schema;