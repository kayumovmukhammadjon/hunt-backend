import { Router } from "express";
import paymeAuth from "../../../middlewares/paymeAuth";

import PaymentController from "../controllers/index";

const router = Router({ mergeParams: true });
const ctrl = PaymentController;


router
    .route("/payme")
    .post(paymeAuth, ctrl.payme);

export default router;

