import { Request, Response } from "express";
import CountryServcei from "../service/service";

class RegionController {
  async getRegions(req: Request, res: Response) {
    try {
      const query: any = req.query;

      const {
        code,
        message,
        data: result,
      } = await CountryServcei.getRegions(query);

      res.status(code).send({
        code,
        msg: message,
        data: result,
      });
    } catch (error) {
      res.status(500).send({ msg: "SERVER_ERROR", data: null });
      throw new Error(`Task controller [create] error: ${error}`);
    }
  }

  async getRegion(req: Request, res: Response) {
    try {
      const query: any = req.query;

      const {
        code,
        message,
        data: result,
      } = await CountryServcei.getRegion(query);

      res.status(code).send({
        code,
        msg: message,
        data: result,
      });
    } catch (error) {
      res.status(500).send({ msg: "SERVER_ERROR", data: null });
      throw new Error(`Task controller [getRegion] error: ${error}`);
    }
  }
}

export default new RegionController();
