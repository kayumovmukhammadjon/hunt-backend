import RegionRepo from "../repo/repo";
import {
  TGetRegion,
  TGetRegionById,
  TGetRegionByName,
  TGetRegions,
} from "./types";

import CountryService from "../../country/service/service";
import { fail, ok } from "../../../utils/response"
import { configurations } from "../../../config";
import logger from "../../../shared/logger";

const repo = new RegionRepo();

class RegionService {
  async getRegionById(data: TGetRegionById) {
    try {
      const { id } = data;

      return repo.getRegionById({ id });
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "region.service",
          method: "GET",
          methodName: "getRegionById",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [region.service] getRegionById"
      );
      console.log(`ERROR: [region.service] getRegionById: ${error}`);
      throw error;
    }
  }


  async getRegionsByIds(regionIds:string[]){
    try{
      return await repo.getRegionsByIds(regionIds);
    }catch(error:any){
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "region.service",
          method: "GET",
          methodName: "getRegionsByIds",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: regionIds,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [region.service] getRegionsByIds"
      );
      console.log(`ERROR: [region.service] getRegionsByIds: ${error}`);
      throw error;
    }
  }

  async getRegions(data: TGetRegions) {
    try {
      const { countryId } = data;

      const country = await CountryService.getCountryById({ id: countryId });

      if (!country) {
        return fail(409, "Country doesn't exist");
      }
      data.country = country;

      const regions = await repo.getRegions(data);

      return ok(regions);
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "region.service",
          method: "GET",
          methodName: "getRegions",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: `regions`,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [region.service] getRegions"
      );
      console.log(`ERROR: [region.service] getRegions: ${error}`);
      return fail(500, error);
    }
  }

  async getRegionByName(data: TGetRegionByName) {
    try {
      const { name } = data;

      return repo.getRegionByName({ name });
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "region.service",
          method: "GET",
          methodName: "getRegionById",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [region.service] getRegionById"
      );
      console.log(`ERROR: [region.service] getRegionById: ${error}`);
      throw error;
    }
  }

  async getRegion(data: TGetRegion) {
    try {
      const { regionId } = data;

      const region = await repo.getRegionById({ id: regionId });
      return ok(region);
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "region.service",
          method: "GET",
          methodName: "getRegion",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [region.service] getRegion"
      );
      console.log(`ERROR: [region.service] getRegion: ${error}`);
      throw error;
    }
  }
}

export default new RegionService();
