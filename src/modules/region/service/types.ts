import { ICountry } from "../../../domain/entities/Country";

export type TGetRegionById = {
  id: string;
};

export type TGetRegions = {
  country: ICountry;
  countryId: string;
};

export type TGetRegionByName = {
  name: string;
};

export type TGetRegion = {
  regionId: string;
};