import { ICountry } from "../../../domain/entities/Country";

export type TGetRegionById = {
  id: string;
};

export type TGetRegions = {
  country: ICountry;
};

export type TGetRegionByName = {
  name: string;
};

