import { configurations } from "../../../config";
import Region from "../../../database/models/Region";
import logger from "../../../shared/logger";
import { TGetRegionById, TGetRegionByName, TGetRegions } from "./types";

class RegionRepo {
  async getRegionById(data: TGetRegionById) {
    try {
      const { id } = data;
      return await Region.findOne({ _id: id, isDeleted: false });
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          repoName: "region.repo",
          method: "GET",
          methodName: "getRegionById",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [region.repo] getRegionById"
      );
      console.log(`ERROR: [region.repo] getRegionById: ${error}`);
      throw error;
    }
  }

  async getRegions(data: TGetRegions) {
    try {
      return await Region.find({ country: data.country.id, isDeleted: false });
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          repoName: "region.repo",
          method: "GET",
          methodName: "getRegions",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: `regions`,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [region.repo] getRegions"
      );
      console.log(`ERROR: [region.repo] getRegions: ${error}`);
      throw error;
    }
  }

  async getRegionByName(data: TGetRegionByName) {
    try {
      const { name } = data;
      return await Region.findOne({ name: name, isDeleted: false });
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          repoName: "region.repo",
          method: "GET",
          methodName: "getRegionById",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [region.repo] getRegionById"
      );
      console.log(`ERROR: [region.repo] getRegionById: ${error}`);
      throw error;
    }
  }

  async getRegionsByIds(regionIds:string[]){
    try{
      return await Region.find({_id:{$in:regionIds}});
    }catch(error:any){
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          repoName: "regions.repo",
          method: "GET",
          methodName: "getRegionsByIds",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: regionIds,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [files.repo] getRegionsByIds"
      );
      console.log(`ERROR: [files.repo] getRegionsByIds: ${error}`);
      throw error;
    }
  }
}

export default RegionRepo;
