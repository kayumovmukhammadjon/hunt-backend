import { Router } from "express";
import RegionController from "../controllers/index";
import ValidationRequest from "../../../middlewares/ValidationRequest";
import requestSchema from "../validations/index";

const router = Router({ mergeParams: true });

const ctrl = RegionController;

router
  .route("/")
  .get(ValidationRequest(requestSchema.getRegions), ctrl.getRegions);

router
  .route("/region")
  .get(ValidationRequest(requestSchema.getRegion), ctrl.getRegion);
  
export default router;
