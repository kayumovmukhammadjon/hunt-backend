import Joi from "joi";

interface TypeSchema {
  getRegions: Joi.Schema<any>;
  getRegion: Joi.Schema<any>;
}

const schema: TypeSchema = {
  getRegions: Joi.object().keys({
    query: Joi.object()
      .keys({
        countryId: Joi.string()
          .pattern(/^[0-9a-fA-F]{24}$/)
          .required(),
      })
      .required(),
  }),
  getRegion: Joi.object().keys({
    query: Joi.object()
      .keys({
        regionId: Joi.string()
          .pattern(/^[0-9a-fA-F]{24}$/)
          .required(),
      })
      .required(),
  }),
};

export default schema;
