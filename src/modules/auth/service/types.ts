export type TVerifyUser = {
    phoneNumber: string
    code: number
}

export type TLoginUser = {
    username: string;
    phoneNumber: string;
    fullName:string;
    chatId:string;
}
