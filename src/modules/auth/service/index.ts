import { TLoginUser, TVerifyUser } from './types'
import UserService from "../../users/services/index";
import { fail, ok } from '../../../utils/response'
import { generateToken } from '../../../utils/jwt'
import SmsService from "../../../integration/sms/index";
import { configurations } from '../../../config'
const {
    accessTokenExpiresIn,
    accessTokenSecret,
    refreshTokenExpiresIn,
    refreshTokenSecret,
} = configurations.jwt

import generateOtpCode from '../../../utils/generateCode'
import TokenService from '../../auth/submodules/tokens/service/index'
import { IGetUserByToken } from '../../../domain/entities/Token'

class AuthService {
    async verifyUser(data: TVerifyUser) {
        const { phoneNumber, code } = data

        const user = await UserService.getUserByPhoneNumber({ phoneNumber })

        if (!user) {
            return fail(404, 'User not found!')
        }

        if (user.verificationCode != code) {
            return fail(400, 'Code is not correct!')
        }

        const payLoad = {
            userId: user.id,
        }

        const accessToken = generateToken(
            payLoad,
            accessTokenSecret,
            Number(accessTokenExpiresIn)
        )

        const refreshToken = generateToken(
            payLoad,
            refreshTokenSecret,
            Number(refreshTokenExpiresIn)
        )

        const token = await TokenService.createToken({
            accessToken,
            refreshToken,
            accessTokenExpireTime: new Date(Date.now() + Number(accessTokenExpiresIn)*60 * 1000),
            refreshTokenExpireTime: new Date(Date.now() + Number(refreshTokenExpiresIn)*60 * 1000),
            user,
        })

        const updatedUser = await UserService.updateUserVerification({
            userId: user.id,
            verified: true,
        })

        return ok({
            userId: user.id,
            accessToken,
            refreshToken,
        })
    }

    async login(data: TLoginUser) {
        const { phoneNumber,username, fullName,chatId } = data

        let user = await UserService.getUserByPhoneNumber({ phoneNumber });

        if (!user) {
            const otpCode = generateOtpCode()

            if(configurations.application.env == 'production'){
                const smsContent = `HuntMe - Confirmation code: ${otpCode}`
                await SmsService.sendSms(phoneNumber, smsContent)
            }

            const telegramContent = `Code: ${otpCode} \nPhone number: ${phoneNumber}`

            await SmsService.sendTelegramMessage(telegramContent);

            const newUser = await UserService.createUser({
                phoneNumber,
                username,
                fullName,
                verificationCode: otpCode,
                verificationCodeExpireTime: new Date(Date.now() + 60000),
                chatId
            });


            return ok(
                'Sent sms code to your phone number! Verification code will be expire 1 minute'
            )
        }


        if(username !== user.username || fullName !== user.fullName){
            
            await UserService.updateFullNameAndUsername({
                userId:user.id,
                username,
                fullName,
            })
        }

        const otpCode = generateOtpCode()

        if(configurations.application.env == 'production'){
            const smsContent = `HuntMe - Confirmation code: ${otpCode}`
            await SmsService.sendSms(phoneNumber, smsContent)
        }

        const telegramContent = `Code: ${otpCode} \nPhone number: ${phoneNumber}`
        await SmsService.sendTelegramMessage(telegramContent)


        await UserService.updateUserVerificationCode({
            phoneNumber,
            code: otpCode,
        })

        return ok(
            'Sent sms code to your phone number! Verification code will be expire 1 minute'
        )
    }
}

export default new AuthService()
