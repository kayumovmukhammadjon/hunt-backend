import Joi from 'joi'

interface TypeSchema {
    loginUser: Joi.Schema<any>;
    verifyUser: Joi.Schema<any>;
}

const schema: TypeSchema = {
    loginUser: Joi.object().keys({
        body: Joi.object().keys({
            fullName:Joi.string().optional(),
            phoneNumber: Joi.string()
                .pattern(/^(?:[0-9] ?){6,14}[0-9]$/)
                .optional(),
            username: Joi.string().optional(),
            chatId:Joi.string().required(),
        }),
    }),
    verifyUser: Joi.object().keys({
        body: Joi.object().keys({
            phoneNumber: Joi.string().required(),
            code: Joi.number().required(),
        }),
    }),
}

export default schema
