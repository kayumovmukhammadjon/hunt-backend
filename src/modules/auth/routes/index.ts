import { Router } from 'express'
import AuthController from '../controllers/index'
import ValidationRequest from '../../../middlewares/ValidationRequest'
import requestSchema from '../validations/index'
import auth from '../../../middlewares/Auth'
import TokenRoutes from '../submodules/tokens/routes/index'

const router = Router({ mergeParams: true })

const ctrl = AuthController

router.use('/token', TokenRoutes)

router
    .route('/verify')
    .post(ValidationRequest(requestSchema.verifyUser), ctrl.verify)

router
    .route('/login')
    .post(ValidationRequest(requestSchema.loginUser), ctrl.login)





export default router


