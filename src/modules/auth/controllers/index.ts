import AuthService from '../service/index'
import { Request, Response } from 'express'
class AuthController {
    async verify(req: Request, res: Response) {
        try {
            const {
                code,
                message,
                data: result,
            } = await AuthService.verifyUser(req.body)

            res.status(code).send({
                code,
                msg: message,
                data: result,
            })
        } catch (error) {
            res.status(500).send({ msg: 'SERVER_ERROR', data: null })
            throw new Error(`Verification error: ${error}`)
        }
    }

    async login(req: Request, res: Response) {
        try {
            const {
                code,
                message,
                data: result,
            } = await AuthService.login(req.body)

            res.status(code).send({
                code,
                msg: message,
                data: result,
            })
        } catch (error) {
            res.status(500).send({ msg: 'SERVER_ERROR', data: null })
            throw new Error(`Login error: ${error}`)
        }
    }
}

export default new AuthController()
