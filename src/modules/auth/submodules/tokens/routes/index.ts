import { Router } from "express";
import TokenController from "../controller";

const router = Router();

const ctrl = TokenController;

router.put("/refresh",ctrl.refreshToken);

export default router;