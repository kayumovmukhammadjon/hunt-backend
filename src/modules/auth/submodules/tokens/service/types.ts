import { IUser } from '../../../../../domain/entities/User'

export type TCreateToken = {
    refreshToken: string
    accessToken: string
    accessTokenExpireTime: Date
    refreshTokenExpireTime: Date
    user: IUser
}

export type TUpdateToken = TCreateToken

export type TGetAccessTokenByRefreshToken = {
    refreshToken: string
}

export type TUpdateAccessTokenByRefreshToken = {
    refreshToken: string
    accessToken: string
}


export type TGetTokenByUserId = {
    userId:string;
}