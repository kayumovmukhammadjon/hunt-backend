import TokenRepo from '../repo'
import {
    TCreateToken,
    TGetTokenByUserId,
    TUpdateAccessTokenByRefreshToken,
    TUpdateToken
} from './types'
import { generateToken } from '../../../../../utils/jwt'
import {ok,fail} from "../../../../../utils/response"
import UserService from "../../../../users/services/index";
import { configurations } from '../../../../../config'
const {
    accessTokenExpiresIn,
    accessTokenSecret,
} = configurations.jwt

const repo = new TokenRepo()

class TokenService {
    async createToken(data: TCreateToken) {
        const { accessToken, refreshToken, user,accessTokenExpireTime,refreshTokenExpireTime } = data

        return await repo.createToken({
            accessToken,
            refreshToken,
            refreshTokenExpireTime,
            accessTokenExpireTime,
            user,
        })
    }


    async updateAccessTokenByRefreshToken(
        data: TUpdateAccessTokenByRefreshToken
    ) {
        const { refreshToken } = data

        const token = await repo.getByRefreshToken({
            refreshToken,
        })

        if (!token) {
            return fail(404, 'Token not found!')
        }

        if (token.refreshTokenExpireTime < new Date()) {
            return fail(401, 'Token is expired!')
        }

        const user = await UserService.getUserById({
            userId:token.user.id
        })

        if (!user) {
            return fail(404, 'User not found!')
        }

        const paylod = {
            userId: user.id,
        }

        const accessToken = generateToken(
            paylod,
            accessTokenSecret,
            Number(accessTokenExpiresIn)
        );


        const updatedAccessToken = await repo.updateAccessTokenByTokenId({
            token,
            accessToken,
            accessTokenExpireTime:new Date(Date.now() + Number(accessTokenExpiresIn)*60 * 1000)
        })

        if (!updatedAccessToken) {
            return fail(500, 'Something went wrong!')
        }

       return ok({
            userId: user.id,
            accessToken:accessToken
        })    
    }

    async updateToken(data: TUpdateToken) {
        const { accessToken, refreshToken, user,accessTokenExpireTime,refreshTokenExpireTime } = data

        return await repo.updateToken({
            accessToken,
            refreshToken,
            user,
            accessTokenExpireTime,
            refreshTokenExpireTime
        })
    }

    async getTokenByUserId(data:TGetTokenByUserId){
        const {userId} = data

        return await repo.getTokenByUserId({
            userId
        })
    }
}

export default new TokenService()
