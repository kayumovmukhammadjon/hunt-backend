import {
    TCreateToken,
    TUpdateAccessTokenById,
    TGetByRefreshToken,
    TUpdateToken,
    TGetTokenByUserId,
} from './types'
import Token from '../../../../../database/models/Token'

class TokenRepo {
    async createToken(data: TCreateToken) {
        const { accessToken, refreshToken, user,refreshTokenExpireTime,accessTokenExpireTime } = data
        
        const newToken = new Token({
            accessToken,
            refreshToken,
            refreshTokenExpireTime,
            accessTokenExpireTime,
            user,
        })

        return await newToken.save()
    }

    async getByRefreshToken(data: TGetByRefreshToken) {
        const { refreshToken } = data

        return await Token.findOne({
            refreshToken,
        })
    }

    async updateAccessTokenByTokenId(data:TUpdateAccessTokenById){
        const {token,accessToken,accessTokenExpireTime} = data

        return await Token.findByIdAndUpdate(
            {
                _id:token.id,
            },
            {
                accessToken,
                accessTokenExpireTime
            }
        )
    }

    async updateToken(data: TUpdateToken) {
        const { accessToken, refreshToken,refreshTokenExpireTime,accessTokenExpireTime, user, } = data

        return await Token.findOneAndUpdate(
            {
                user: user.id,
            },
            {
                accessToken,
                refreshToken,
                refreshTokenExpireTime,
                accessTokenExpireTime,
            }
        )
    }

    async getTokenByUserId(data: TGetTokenByUserId) {
        const {userId} = data

        return await Token.findOne({
            user:userId
        })
    }
}

export default TokenRepo
