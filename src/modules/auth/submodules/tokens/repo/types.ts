import { IToken } from '../../../../../domain/entities/Token'
import { IUser } from '../../../../../domain/entities/User'

export type TCreateToken = {
    refreshToken: string
    accessToken: string
    accessTokenExpireTime: Date
    refreshTokenExpireTime: Date
    user: IUser
}

export type TUpdateToken = TCreateToken

export type TGetByRefreshToken = {
    refreshToken: string
}

export type TUpdateAccessTokenById = {
    token: IToken
    accessToken: string
    accessTokenExpireTime: Date
}

export type TGetTokenByUserId = {
    userId:string;
}