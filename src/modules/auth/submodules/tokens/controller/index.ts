import { Response,Request } from "express";
import TokenService from "../service";

class TokenController {
    async refreshToken(req:Request,res:Response){
        try{
            const {
                code,
                message,
                data:result
            } = await TokenService.updateAccessTokenByRefreshToken(req.body);

            return res.status(code).json({
                message,
                data:result
            })

        }catch(error){
            res.status(500).send({ msg: "SERVER_ERROR", data: null });
            throw new Error(`Token controller [create] error: ${error}`);
        }
    }
}

export default new TokenController();