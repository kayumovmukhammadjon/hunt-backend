export type TGetCurrencyById = {
  currencyId: string;
};

export type TGetCurrency = {
  id: string;
};
