import CurrencyRepository from "../repo/index";
import { ICurrency } from "../../../domain/entities/Currency";

import { fail, ok } from "../../../utils/response";
import { TGetCurrency, TGetCurrencyById } from "./types";
import { configurations } from "../../../config";
import logger from "../../../shared/logger";

const repo = new CurrencyRepository();

class CurrencyService {
  async getAllCurrencies() {
    try {
      const currencies: ICurrency[] = await repo.getAllCurrencies();

      return ok(currencies);
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "currency.service",
          method: "GET",
          methodName: "getAllCurrencies",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: `currencies`,
          apiData: {
            body: null,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [currency.service] getAllCurrencies"
      );
      console.log(`ERROR: [currency.service] getAllCurrencies: ${error}`);
      return fail(500, error);
    }
  }

  async getCurrencyById(data: TGetCurrencyById) {
    try {
      const { currencyId } = data;

      return await repo.getCurrencyById({ currencyId });
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "currency.service",
          method: "GET",
          methodName: "getCurrencyById",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [currency.service] getCurrencyById"
      );
      console.log(`ERROR: [currency.service] getCurrencyById: ${error}`);
      throw error;
    }
  }

  async getCurrency(data: TGetCurrency) {
    try {
      const { id } = data;

      const currency = await repo.getCurrencyById({ currencyId: id });

      if (!currency) {
        return fail(409, "Currency doesn't exists");
      }

      return ok(currency);
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "currency.service",
          method: "GET",
          methodName: "getCurrency",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: `currencies/currency`,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [currency.service] getCurrency"
      );
      console.log(`ERROR: [currency.service] getCurrency: ${error}`);
      return fail(500, error);
    }
  }
}

export default new CurrencyService();
