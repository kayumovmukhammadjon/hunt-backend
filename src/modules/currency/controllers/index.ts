import { Request, Response } from "express";
import CurrencyService from "../services/index";

class CurrencyController {
  async getAllCurrencies(req: Request, res: Response) {
    try {
      const {
        code,
        message,
        data: result,
      } = await CurrencyService.getAllCurrencies();

      res.status(code).send({
        code,
        msg: message,
        data: result,
      });
    } catch (error) {
      res.status(500).send({ msg: "SERVER_ERROR", data: null });
      throw new Error(`Currency controller [create] error: ${error}`);
    }
  }

  async getCurrency(req: Request, res: Response) {
    try {
      const query: any = req.query;

      const {
        code,
        message,
        data: result,
      } = await CurrencyService.getCurrency(query);
      
      res.status(code).send({
        code,
        msg: message,
        data: result,
      });
    } catch (error) {
      res.status(500).send({ msg: "SERVER_ERROR", data: null });
      throw new Error(`Currency  controller [create] error: ${error}`);
    }
  }
}

export default new CurrencyController();
