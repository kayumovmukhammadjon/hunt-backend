import Joi from "joi";

interface TypeSchema {
  getCurrency: Joi.Schema<any>;
}

const schema: TypeSchema = {
  getCurrency: Joi.object().keys({
    query: Joi.object().keys({
      id: Joi.string()
        .pattern(/^[0-9a-fA-F]{24}$/)
        .required(),
    }),
  }),
};

export default schema;
