import { configurations } from "../../../config";
import Currency from "../../../database/models/Currency";
import logger from "../../../shared/logger";
import { TGetCurrencyById } from "./types";

class CurrencyRepository {
  async getAllCurrencies() {
    try {
      const currencies = await Currency.find({ isDeleted: false });

      return currencies;
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          repoName: "currency.repo",
          method: "GET",
          methodName: "getAllCurrencies",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: `currencies`,
          apiData: {
            body: null,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [currency.repo] getAllCurrencies"
      );
      console.log(`ERROR: [currency.repo] getAllCurrencies: ${error}`);
      throw error;
    }
  }

  async getCurrencyById(data: TGetCurrencyById) {
    try {
      const { currencyId } = data;

      return await Currency.findOne({ _id: currencyId });
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          repoName: "currency.repo",
          method: "GET",
          methodName: "getCurrencyById",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: data,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [currency.repo] getCurrencyById"
      );
      console.log(`ERROR: [currency.repo] getCurrencyById: ${error}`);
      throw error;
    }
  }
}

export default CurrencyRepository;
