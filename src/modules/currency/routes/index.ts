import { Router } from "express";
import CurrencyController from "../controllers/index";
import ValidationRequest from "../../../middlewares/ValidationRequest";
import RequestSchema from '../validations/index'

const router = Router({ mergeParams: true });

const ctrl = CurrencyController;

router.route("/").get(ctrl.getAllCurrencies);

router.route('/currency').get(ValidationRequest(RequestSchema.getCurrency),ctrl.getCurrency)

export default router;
