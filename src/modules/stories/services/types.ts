import { IUser } from "../../../domain/entities/User";

export type TCreateUser = {
    fullName:string;
    username:string;
    phoneNumber:string;
    verificationCode: number
    verificationCodeExpireTime: Date
}


export type TGetUserByPhoneNumber = {
    phoneNumber: string
}

export type TUpdateVerificationCode = {
    phoneNumber: string;
    code: number;
  };

export type TVerifyUserByCode = {
    phoneNumber: string
    code: number
}

export type TGetUserById = {
    userId:string;
}


export type TUpdateUser = {
    userId:string;
    user:IUser;
    phoneNumber:string;
    username:string;
    fullName:string;
}

export type TDeleteUser = {
    userId:string;
    user:IUser;
}

export type TGetAllUsers = {
    skip:string;
    limit:string;
}

export type TUpdateFullNameAndUsername = {
    userId:string;
    fullName:string;
    username:string;
}



export type TUpdateUserVerification = {
    userId: string,
    verified: boolean
}
