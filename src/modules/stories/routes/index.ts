import { Router } from "express";
import ValidationRequest from "../../../middlewares/ValidationRequest";
import requestSchema from "../validations/index";
import auth from "../../../middlewares/Auth";

import UserController from "../controllers/index";


const router = Router({mergeParams:true});
const ctrl = UserController;

router
    .route("/")
    .get(ValidationRequest(requestSchema.getAllUsers),auth,ctrl.getAllUsers)
    .put(ValidationRequest(requestSchema.updateUser),auth,ctrl.updateUser)
    .delete(ValidationRequest(requestSchema.deleteUser),auth,ctrl.deleteUser)

router
    .route("/user")
    .get(ValidationRequest(requestSchema.getUser),auth,ctrl.getUser);

export default router;