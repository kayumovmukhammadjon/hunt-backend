import { IUser } from "../../../domain/entities/User";

export type TCreateUser = {
    fullName:string;
    username:string;
    phoneNumber:string;
    verificationCode: number
    verificationCodeExpireTime: Date
}


export type TGetUserByPhoneNumber = {
    phoneNumber: string
}

export type TUpdateVerificationCode = {
    phoneNumber: string;
    code: number;
  };

export type TVerifyUserByCode = {
    phoneNumber: string
    code: number
}

export type TGetUserById = {
    userId:string;
}

export type TUpdateUser = {
    user:IUser
    phoneNumber:string;
    username:string;
    fullName:string;
}

export type TDeleteUser = {
    user:IUser;
}

export type TUpdateFullNameAndUsername = {
    userId:string;
    fullName:string;
    username:string;
}

export type TGetAllUsers = {
    skip:string;
    limit:string;
}

export type TUpdateUserVerification = {
    userId: string,
    verified: boolean
}

