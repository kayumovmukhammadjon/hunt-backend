import User from "../../../database/models/User";
import { TCreateUser, TDeleteUser, TGetAllUsers, TGetUserById, TGetUserByPhoneNumber, TUpdateFullNameAndUsername, TUpdateUser, TUpdateUserVerification, TUpdateVerificationCode } from "./types";

class UserRepo {
    async create(data:TCreateUser){
        const newUser = new User(data)

        await newUser.save()

        return newUser;
    }

    async getUserById(data:TGetUserById){
        return await User.findOne({
            _id:data.userId,
            isDeleted:false
        })
    }

    async getAllUsers(data:TGetAllUsers){
        return await User.find({isDeleted:false}).skip(+data.skip).limit(+data.limit);
    }

    async updateUser(data:TUpdateUser){
        return await User.findOneAndUpdate({
            _id:data.user.id,
            isDeleted:false
        },data,{new:true})
    } 

    async getUserByPhoneNumber(data:TGetUserByPhoneNumber){
        return await User.findOne({
            phoneNumber:data.phoneNumber,
            isDeleted:false
        })
    }

    async deleteUser(data:TDeleteUser){
        return await User.findOneAndUpdate({
            _id:data.user.id,
            isDeleted:false
        },{
            isDeleted:true
        })
    }

    async updateFullNameAndUsername(data:TUpdateFullNameAndUsername){
        const {userId,fullName,username} = data;
        return await User.findOneAndUpdate({
            _id:userId,
        },{
            username,
            fullName
        })
    }

    async updateUserVerificationCode(data: TUpdateVerificationCode) {
        const { code, phoneNumber } = data;
    
        const currentDate = new Date();
    
        return await User.findOneAndUpdate(
            { phoneNumber, isDeleted: false },
            {
                verificationCode: code,
                verificationCodeExpireTime: new Date(
                currentDate.getTime() + 1 * 60 * 1000
                ),
            }
        );
    }

    async updateUserVerification(data:TUpdateUserVerification){
        const {userId, verified} = data;

        return await User.findOneAndUpdate({
            _id:userId,
            isDeleted:false,
        },{
            verified,
        },{
            new:true,
        })
    }

}

export default UserRepo;