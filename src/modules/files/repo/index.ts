import { configurations } from "../../../config";
import File from "../../../database/models/File";
import logger from "../../../shared/logger";

class FileRepository {
  async upload(files: {
    originalName: string;
    filename: string;
    mimetype: string;
    extension: string;
    size: number;
  }) {
    try {
      const newFile = new File(files);

      await newFile.save();

      return newFile;
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          repoName: "files.repo",
          method: "POST",
          methodName: "upload",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: `files`,
          apiData: {
            body: files,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [files.repo] upload"
      );
      console.log(`ERROR: [files.repo] upload: ${error}`);
      throw error;
    }
  }

  async getFileById(fileId: string) {
    try {
      return await File.findOne({ _id: fileId });
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          repoName: "files.repo",
          method: "GET",
          methodName: "getFileById",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: fileId,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [files.repo] getFileById"
      );
      console.log(`ERROR: [files.repo] getFileById: ${error}`);
      throw error;
    }
  }

  async getFilesByIds(fileIds: string[]) {
    try {
      return await File.find({ _id: { $in: fileIds } });
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          repoName: "files.repo",
          method: "GET",
          methodName: "getFilesByIds",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: fileIds,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [files.repo] getFilesByIds"
      );
      console.log(`ERROR: [files.repo] getFilesByIds: ${error}`);
      throw error;
    }
  }
}

export default new FileRepository();
