import { Request, Response } from "express";
import FileService from "../service/index";
import axios from "axios";

class FileController {
  async uploadFile(req: Request, res: Response) {
    try {
      const files = req.files;

      const {
        code,
        message,
        data: result,
      } = await FileService.uploadFile(files);

      res.status(code).send({
        code,
        msg: message,
        data: result,
      });
    } catch (error) {
      res.status(500).send({ msg: "SERVER_ERROR", data: null });
      throw new Error(`Task controller [create] error: ${error}`);
    }
  }

  async uploadMultipleFile(req: Request, res: Response) {
    try {
      const files: any = req.files;
      const apiResponse = [];

      if(Array.isArray(files.files)){
        for (let i = 0; i < files.files.length; i++) {
          const fileData = files.files[i];

          let input = {
            files: fileData,
          };

          const { data: result } = await FileService.uploadFile(input);

          apiResponse.push(result);
        }
      }else{
        const input = {
          files: files.files,
        }
        const { data: result } = await FileService.uploadFile(input);

        apiResponse.push(result);
      }

      res.status(200).send({
        code: 200,
        msg: "Completed",
        data: apiResponse,
      });
    } catch (error) {
      res.status(500).send({ msg: "SERVER_ERROR", data: null });
      throw new Error(`Task controller [create] error: ${error}`);
    }
  }

  async getFile(req: Request, res: Response) {
    try {
      const query: any = req.query;

      const {
        code,
        message,
        data: result,
      } = await FileService.getFile(query.fileId);

      res.status(code).send({
        code,
        msg: message,
        data: result,
      });
    } catch (error) {
      res.status(500).send({ msg: "SERVER_ERROR", data: null });
      throw new Error(`Task controller [create] error: ${error}`);
    }
  }

  async getFilePipe(req: Request, res: Response) {
    try {
      const query: any = req.query;

      const {
        code,
        message,
        data: result,
      } = await FileService.getFile(query.fileId);

      const fileUrl = result.filename;

      const response = await axios.get(fileUrl, { responseType: "stream" });

      res.setHeader("Content-Type", response.headers["content-type"]);
      res.setHeader(
        "Content-Disposition",
        'attachment; filename="downloaded.jpeg"'
      );

      response.data.pipe(res);
    } catch (error) {
      res.status(500).send({ msg: "SERVER_ERROR", data: null });
      throw new Error(`Task controller [create] error: ${error}`);
    }
  }

  async getFileUrl(req: Request, res: Response) {
    try {
      const query: any = req.query;

      const {
        code,
        message,
        data: result,
      } = await FileService.getFile(query.fileId);

      res.status(code).send(result.filename);
    } catch (error) {
      res.status(500).send({ msg: "SERVER_ERROR", data: null });
      throw new Error(`Task controller [create] error: ${error}`);
    }
  }
}

export default new FileController();
