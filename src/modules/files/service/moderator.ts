import sharp from "sharp";

interface InputFile {
  file: Buffer;
}

class ImageModerator {
  private imageSizes = [
    {
      width: 160,
      height: 90,
      name: "image0",
    },
    {
      width: 320,
      height: 180,
      name: "image1",
    },
    {
      width: 640,
      height: 360,
      name: "image2",
    },
  ];

  async resizeImage(input: InputFile) {
    const images = [];

    for (let i = 0; i < this.imageSizes.length; i++) {
      const element = this.imageSizes[i];

      const result = await sharp(input.file)
        .resize({
          width: element.width,
          height: element.height,
          fit: "cover",
        })
        .toBuffer();

      images.push({
        size: `${element.width}x${element.height}`,
        buffer: result,
      });
    }

    return images;
  }
}

export default new ImageModerator();
