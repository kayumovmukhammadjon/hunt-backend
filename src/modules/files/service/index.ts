import { fail, ok } from "../../../utils/response";
import { S3, PutObjectCommand } from "@aws-sdk/client-s3";
import { v4 as uuid } from "uuid";
import { configurations } from "../../../config/index";
import * as mime from "mime-types";
import { mimetypes as allowedExtensions } from "../constants/mimetypes";
import imageModerator from "./moderator";

import FileRepository from "../repo";
import logger from "../../../shared/logger";

class FileService {
  async uploadFile(files: any) {
    try {
      let file = files.files;

      if (!file || file.data.length === 0) {
        return fail(400, "No files were uploaded.");
      }

      const s3Client = new S3({
        forcePathStyle: false,
        endpoint: configurations.awsService.endpoint,
        region: configurations.awsService.region,
        credentials: {
          accessKeyId: configurations.awsService.accessKey,
          secretAccessKey: configurations.awsService.secretKey,
        },
      });

      const uniqeName = uuid() + "." + mime.extension(file.mimetype);

      s3Client.send(
        new PutObjectCommand({
          Bucket: configurations.awsService.name,
          Key: `${uniqeName}`,
          Body: files.files.data,
          ACL: "public-read",
        })
      );

      const customizedFiles = [];

      const extension: string = this.getExtension(file.mimetype);

      if (
        Array.isArray(allowedExtensions) &&
        !allowedExtensions.includes(extension)
      ) {
        return fail(400, `File extension ${extension} is not allowed`);
      }

      customizedFiles.push({
        originalName: file.name,
        filename: `${configurations.awsService.url}/${uniqeName}`,
        mimetype: file.mimetype,
        extension: "",
        size: file.size,
      });

      const originalImage = await FileRepository.upload(customizedFiles[0]);

      return ok(originalImage);
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "files.service",
          method: "POST",
          methodName: "uploadFile",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: `files`,
          apiData: {
            body: null,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [files.service] uploadFile"
      );
      console.log(`ERROR: [files.service] uploadFile: ${error}`);
      return fail(500, error);
    }
  }

  saveFileCridentials(files: any) {
    try {
      return FileRepository.upload(files);
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "files.service",
          method: "POST",
          methodName: "saveFileCridentials",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: files,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [files.service] saveFileCridentials"
      );
      console.log(`ERROR: [files.service] saveFileCridentials: ${error}`);
      throw error;
    }
  }

  getFileById(fileId: string) {
    try {
      return FileRepository.getFileById(fileId);
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "files.service",
          method: "GET",
          methodName: "getFileById",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: fileId,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [files.service] getFileById"
      );
      console.log(`ERROR: [files.service] getFileById: ${error}`);
      throw error;
    }
  }

  async getFile(fileId: any) {
    try {
      const file = await FileRepository.getFileById(fileId);

      if (!file) {
        return fail(409, "File doesnt exists");
      }

      return ok(file);
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "files.service",
          method: "GET",
          methodName: "getFile",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: `files`,
          apiData: {
            body: fileId,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [files.service] getFile"
      );
      console.log(`ERROR: [files.service] getFile: ${error}`);
      return fail(500, error);
    }
  }

  getFilesByIds(fileIds: string[]) {
    try {
      return FileRepository.getFilesByIds(fileIds);
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "files.service",
          method: "GET",
          methodName: "getFilesByIds",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: fileIds,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [files.service] getFilesByIds"
      );
      console.log(`ERROR: [files.service] getFilesByIds: ${error}`);
      throw error;
    }
  }

  private getExtension(mimetype: string): string {
    try {
      let result = mime.extension(mimetype);

      if (!result) result = "";

      return result;
    } catch (error: any) {
      logger.error(
        JSON.stringify({
          logLevel: "ERROR",
          projectName: `${configurations.application.name}`,
          serviceName: "files.service",
          method: "GET",
          methodName: "getExtension",
          brandId: null,
          userId: null,
          customerId: null,
          exception: error.name,
          apiPath: null,
          apiData: {
            body: mimetype,
            query: null,
            params: null,
          },
          message: error.message,
        }),
        "ERROR: [files.service] getExtension"
      );
      console.log(`ERROR: [files.service] getExtension: ${error}`);
      throw error;
    }
  }
}

export default new FileService();
